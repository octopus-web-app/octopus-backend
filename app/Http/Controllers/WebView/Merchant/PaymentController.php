<?php

namespace App\Http\Controllers\WebView\Merchant;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Http;
use App\Services\MakePayment;

class PaymentController extends Controller
{
    public function __construct(
        MakePayment $payment_service
    ){
        $this->payment_service = $payment_service;
    }

	//@do_next_time : This Function is still lack of variable checking
	public function verification(Request $request)
	{
		$verification_orders = \App\Models\Shop\Verification\Order::findOrFail($request->verification_order_id);

		$signature = $this->payment_service->getSignature($verification_orders->display_running_no, 1);

		$contents['route'] = 'https://payment.ipay88.com.my/ePayment/entry.asp';
    	
    	$contents['entities'] = [
			'MerchantCode' => env('iPay_MerchantCode'),
			'PaymentId' => '6',
			'RefNo' => $verification_orders->display_running_no,
			'Amount' => 1,
			'Currency' => 'MYR',
			'ProdDesc' => 'Verified Vendor',
			'UserName' => 'Ben',
			'UserEmail' => 'iambenghooi93@gmail.com',
			'UserContact' => '0123456789',
			'Signature' => $signature,
			'Remark' => '',
			'Lang' => 'UTF-8',
			'ResponseURL' => 'http://staging.lameway.com/payment/verification/response',
			'BackendURL' => 'http://staging.lameway.com/backend.php',
		];

     	return view('payment.index', $contents);
	}

	public function response(Request $request)
	{
		$verification_orders = \App\Models\Shop\Verification\Order::where('display_running_no', $request->RefNo)->first();

		if($request['Status'])
		{
			$verification_orders->update([
				'is_payment_completed' => 1,
				'status' => 'P',
			]);
		}

		$this->payment_service->savePaymentLog($request, 1, 'verification_order_id');

		return 'OK Done';
	}
	
}
