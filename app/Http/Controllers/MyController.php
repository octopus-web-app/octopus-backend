<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 

use Maatwebsite\Excel\Facades\Excel;

use File;
use Response;
use Auth;



class MyController extends Controller
{
  
    public function __construct(
    							
    							)
    {  
       
    }

    public function displayImage($filename)
    {    
        $path = storage_path('app/public/verification_docs/'.$filename);

        if (!File::exists($path)) {
            abort(404);
        }    

        $file = File::get($path);
        $type = File::mimeType($path);    

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);   

        return $response;

    }

    public function dashboard(Request $request)
    {
        $contents[] ='';
       
        return view('dashboard', $contents);
    }
    
    public function contactUs(Request $request)
    {
        $contents=[];       
        return view('contact-us', $contents);
    }
    
    public function faq(Request $request)
    {
        $contents=[];       
        return view('faq', $contents);
    }

    public function blankTemplate()
    { 
        /*$content['category'] = $this->feedbackRepo->getCategory();
        $content['id'] = $user_id;
        */
         $content['test'] = 'test 123';
        return view('blank-page', $content);
       // return view('dashboard');
    }

}
