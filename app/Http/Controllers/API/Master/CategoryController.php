<?php

namespace App\Http\Controllers\API\Master;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 

class CategoryController extends Controller
{
	public function getCategories()
	{
        return \App\Models\Master\Category::
        	where('is_enable', 1)
        	->has('sub_categories')
        	->with([
	            'sub_categories' => function ($sub_cateogories_query) {
	                return $sub_cateogories_query->where('is_enable', 1);
			    }
	        ])
        	->get();
	}
}
