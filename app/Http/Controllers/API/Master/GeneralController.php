<?php

namespace App\Http\Controllers\API\Master;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 

class GeneralController extends Controller
{
	public function getOperatingHourDays()
	{
        return config('weekday.weekday_array_format');
	}

	public function getStates()
	{
        return \App\Models\Master\State::select('id', 'name')->with('cities')->get();
	}
}
