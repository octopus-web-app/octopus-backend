<?php

namespace App\Http\Controllers\API\Master;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 

class BankController extends Controller
{
	public function getBanks()
	{
        return \App\Models\Master\Bank::where('is_enable', 1)->get();
	}
}
