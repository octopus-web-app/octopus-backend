<?php

namespace App\Http\Controllers\API\Master;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\Master\ServiceMethod;
use App\Models\Master\SubCategory;

class ServiceController extends Controller
{
	public function getServiceMethods(Request $request)
	{
		if(!$request->master_sub_category_id)
		{
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Error',
                'message' => 'Sub Category is required.'
            ]);
		}

		$sub_category_ids = SubCategory::with('service_methods')->findOrFail($request->master_sub_category_id)->service_methods->pluck('id')->toArray();

		if(empty($sub_category_ids))
		{
	        return ServiceMethod::selectRaw(
	        	'id,name,description,0 as is_enabled'
	        )->get();
		}
		else
		{
	        return ServiceMethod::selectRaw(
	        	'id,name,description,
				CASE
				    WHEN id IN ('.implode(",",$sub_category_ids).') THEN 1
				    ELSE 0
				END as is_enabled'
	        )->get();
		}

	}
}
