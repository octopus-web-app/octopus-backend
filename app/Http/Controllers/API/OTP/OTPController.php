<?php

namespace App\Http\Controllers\API\OTP;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Http;
use App\Http\Requests\Merchant\OTP as OTPRequest; 

class OTPController extends Controller
{
    //@dnt_urgent_3 : This Function is still lack of request checking for otp time update time
    //@dnt_urgent_1 : SMS not send but response 200
	public function sendOTP(OTPRequest $request)
	{
		$mobile_no = \App\Services\Helper::standardize_mobile_phone($request->mobile_no);
		$otp_code = mt_rand(100000, 999999);

		$response = Http::get(
			\App\Services\OTP::generateLink($mobile_no, 'OTP Code : '.$otp_code)
		);

		if($response->ok())
		{
			$otp_model = \App\Models\Core\OTPCode::updateOrCreate([
				'mobile_no' => $mobile_no,
			],[
				'otp_code' => $otp_code,
			]);
			
			return response([
	   			'message' => 'OTP Code has been sent.',
				'otp_code' => $otp_code,
	   		]);
		}
		else
		{
			return response([
	   			'message' => 'Server Error! Please try again later.'
	   		], 500);
		}
	}
}
