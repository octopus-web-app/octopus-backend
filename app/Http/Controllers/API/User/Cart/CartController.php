<?php

namespace App\Http\Controllers\API\User\Cart;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Repositories\Service\ServiceRepo; 
use App\Repositories\Cart\CartRepo; 
use Validator;

class CartController extends Controller
{
    protected $serviceRepo;
    protected $cartRepo;

    public function __construct(
                                ServiceRepo $serviceRepo,
                                CartRepo $cartRepo

                                )
    {
        $this->serviceRepo = $serviceRepo;
        $this->cartRepo = $cartRepo;
    }
   
    public function addToCart(Request $request)
    {

        //$this->validateInput($request->user_id,'user_id');
        $this->validateInput($request->service_id,'service_id');
        $this->validateInput($request->sku_id,'sku_id');
        $this->validateInput($request->quantity,'quantity');
        $this->validateInput($request->booking_date,'booking_date');
        $this->validateInput($request->booking_time,'booking_time');

        $this->cartRepo->addToCart($request);

        // $this->cartRepo->addToCart($request);
        // $response['status'] = 1;
        $cart = $this->getCart();

        $response['message'] = 'Successfully added to cart.' ; 
        $response['cart'] = $cart;
        
        return response()->json($response);
    }

    public function viewCart()
    {
        $cart = $this->getCart();
        $response['message'] = ( count($cart) == 0 ) ? 'Your cart is empty.' : ''; 
        $response['cart'] = $cart ; 
        return response()->json($response);
    }
    
    
    public function getCart()
    {
        $carts = $this->cartRepo->getCart();
        $cart_array[] = '';
        foreach ($carts as $key => $cart) {
            $cart_item['cart'] = $cart;
            $cart_item['service'] = $cart->service;
            $cart_item['images'] = $cart->service->images;
            $cart_item['sku'] = $cart->service->skus;
        }
       
        array_push($cart_array, $cart_item);
        return $cart_array;
   
        //$response['message'] = ( count($cart_array) == 0 ) ? 'Your cart is empty.' : ''; 
        //$response['cart'] = $cart ; 
      
        //return response()->json($response);
    }

    public function validateInput($input,$field)
    {
        if($input === '' || $input === null)
        {
            throw new \App\Exceptions\ApiAuthorizeException([
               // 'title' => 'Failed',
                'message' => $field.' cannot be blank.',
            ]);

        }
    }

    
}
