<?php

namespace App\Http\Controllers\API\User\Profile;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Repositories\User\UserRepo; 

class ProfileController extends Controller
{
    protected $userRepo;

    public function __construct(UserRepo $userRepo)
    {
        $this->userRepo = $userRepo;
    }

	public function getProfile() 
	{

		$user = \Auth::guard('api-user')->user(); 

		return response()->json($user->getApiResponse(), 200);
	}
    
    public function addAddress(Request $request)
    {
        $entity = $this->userRepo->createAddress($request);

        $response['status'] = 1;
        $response['message'] = 'Address updated.';        

        return response()->json($response, 200);
        
    }
}
