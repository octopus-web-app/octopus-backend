<?php

namespace App\Http\Controllers\API\User\Auth;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 

class LoginController extends Controller
{
	public function login(Request $request)
	{
        $validator = \Validator::make($request->all(), [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) 
        {
            return response(['message' => $validator->errors()->messages()], 422);
        }

       	$user = \App\Models\User::where('email', $request->email)->first();

       	if(!$user || !\Hash::check($request->password, $user->password))
       	{
       		return response([
       			'message' => ['These credentials do not match'] 
       		], 422);
       	} 
       	else 
       	{
       		return response([
       			'token' => $user->createToken('my-app-token')->plainTextToken
       		]);
       	}
	}
}
