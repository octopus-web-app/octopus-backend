<?php

namespace App\Http\Controllers\API\User\Service;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Repositories\Service\ServiceRepo; 


class ServiceController extends Controller
{
    protected $serviceRepo;

    public function __construct(ServiceRepo $serviceRepo)
    {
        $this->serviceRepo = $serviceRepo;
    }
   
    
    public function getService(Request $request)
    {
       // return new ServiceResource($request->service);
    }

    public function getServicesBySubCategory(Request $request, $master_sub_category_id)
    {
        $entities = $this->serviceRepo->getServicesBySubCategoryId($request, $master_sub_category_id);
        $services = [];
        foreach ($entities as $entity) {
            $services[] = $entity->getApiResponse();
        }
        $response['status'] = 1;
        $response['message'] = ''; 
        $response['services'] = $services;
        return response()->json($response);
    }

    
}
