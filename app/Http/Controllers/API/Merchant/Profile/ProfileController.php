<?php

namespace App\Http\Controllers\API\Merchant\Profile;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Http\Requests\Merchant\Profile as ProfileRequest; 
use App\Http\Resources\Merchant\MerchantUserResource; 

class ProfileController extends Controller
{
	public function getProfile() 
	{
		$user = \Auth::guard('api-user')->user(); 

		return new MerchantUserResource($user);
	}

    public function update(ProfileRequest $request)
    {
        $user = \Auth::guard('api-user')->user();
 
        \App\Models\Merchant\Profile::updateOrCreate([ 
            'user_id' => $user->id
        ]);

        if($request->password)
        {
            $user->update([ 
                'name' => $request->name,
                'ic_no' => $request->ic_no,
                'dob' => $request->dob,
                'gender' => $request->gender,
                'marital_status' => $request->marital_status,
                'email' => $request->email,
                'password' => \Hash::make($request->password),
            ]);
        }
        else
        {
            $user->update([ 
                'name' => $request->name,
                'ic_no' => $request->ic_no,
                'dob' => $request->dob,
                'gender' => $request->gender,
                'marital_status' => $request->marital_status,
                'email' => $request->email,
            ]);
        }

        return response([
            'title' => 'Message',
            'message' => 'Profile Updated'
        ]);
    }

    public function upload(ProfileRequest $request)
    {
        $request->uploadAuthorize();

        $user = \Auth::guard('api-user')->user();

        $file_category = $request->file_category;

        $photo_path = \App\Services\SaveFile::saveFile('profile_pic', $request->attachment);
        
        if($user->profile->$file_category)
        {
            \App\Services\SaveFile::deleteFile($user->profile->$file_category);
        }
	
    	if($photo_path)
    	{
        	$user->profile->update([
        		$file_category => $photo_path
        	]);
    	}

        return response([
            'title' => 'Message',
            'message' => 'File Uploaded'
        ]);
    }

    public function completed()
    {
        $user = \Auth::guard('api-user')->user();
        
        $user->profile->update([
            'is_profile_completed' => 1
        ]);

        //@dnt_urgent_1 : should send email & verify

        return response([
            'title' => 'Submission Completed',
            'message' => 'We have sent an email to your mailbox. Please click the link to verify your account.'
        ]);
    }
}
