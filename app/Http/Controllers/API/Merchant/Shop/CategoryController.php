<?php

namespace App\Http\Controllers\API\Merchant\Shop;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;
use App\Repositories\Shop\ShopRepo; 
use App\Http\Requests\Merchant\Shop\ShopRequest; 

class CategoryController extends Controller
{
    public function getSelectedSubCatogory(ShopRequest $request, $shop_id)
    {
    	return $request->shop->sub_categories;
    }
}
