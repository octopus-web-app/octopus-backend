<?php

namespace App\Http\Controllers\API\Merchant\Shop;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;
// use App\Repositories\Shop\TeamRepo; 
use App\Http\Requests\Merchant\Shop\TeamRequest; 
use App\Http\Requests\Merchant\Shop\ShopRequest; 
use App\Http\Resources\Merchant\TeamResource; 

class TeamController extends Controller
{
    public function getTeam(TeamRequest $request, $team_id)
    {
        return new TeamResource($request->team);
    }

    public function getTeams(ShopRequest $request)
    {
        return TeamResource::collection($request->shop->teams);
    }

    public function createTeam(ShopRequest $request, $shop_id)
    {
        $team = \App\Models\Merchant\Team\Team::create([
            'shop_id' =>  $shop_id,
            'name' => $request->name,
            'qualification' => $request->qualification,
            'experience_amount' =>  $request->experience_amount,
            'experience_type' =>  $request->experience_type,
        ]);
        
        foreach($request->operating_hours as $operating_hour)
        {
            if(!is_null($operating_hour['day']))
            {
                $operating_hour['team_id'] = $team->id;
                \App\Models\Merchant\Team\OperatingHour::create($operating_hour);
            }
        }

        if($request->user_ids && !empty($request->user_ids))
        {
            $team->users()->sync($request->user_ids);
        }

        return response([
            'message' => 'Team has been created.'
        ]);
    }
}
