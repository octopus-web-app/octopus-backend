<?php

namespace App\Http\Controllers\API\Merchant\Shop;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Http\Resources\Merchant\FirstShopStatusResource; 
use App\Http\Resources\Merchant\ShopResource; 
use App\Http\Requests\Merchant\Shop\ShopRequest; 
use App\Repositories\Shop\ShopRepo; 

class ShopController extends Controller
{
    public function getFirstShopStatus()
    {
        return new FirstShopStatusResource(\Auth::guard('api-user')->user()->first_shop); 
    }
    
    public function getFirstShop()
    {
        $first_shop = \Auth::guard('api-user')->user()->first_shop()->firstOrCreate();

        ShopRepo::firstOrCreateShopUser(
            \Auth::guard('api-user')->user()->id,
            $first_shop->id
        );

        $first_shop->update([
            'is_business_category' => 1
        ]);

        return new ShopResource($first_shop); 
    }

    public function createShop(Request $request)
    {
        $user = \Auth::guard('api-user')->user();

        //@dnt_urgent_1 : check category and service id
        $shop = \App\Models\Shop\Shop::create([
            'owner_user_id' =>  $user->id,
            'master_category_id' => $request->category_id,
            'shop_name' => $request->shop_name,
        ]);

        $shop->sub_categories()->sync($request->sub_category_ids);

        $request->shop->update([
            'is_business_category' => 1
        ]);

        ShopRepo::firstOrCreateShopUser(
            \Auth::guard('api-user')->user()->id,
            $shop->id
        );

        return new ShopResource($shop); 
    }

    public function getShop(ShopRequest $request, $shop_id)
    {
        return new ShopResource($request->shop); 
    }

    public function getShopCoverageShop(ShopRequest $request, $shop_id)
    {
        return ShopRepo::getCoverageAreas(
            $request->shop
        );
    }

    public function updateCategory(ShopRequest $request, $shop_id)
    {
        $request->shop->update([
            'master_category_id' => $request->category_id,
            'name' => $request->shop_name,
        ]);

        $request->shop->sub_categories()->sync($request->sub_category_ids);

        return response([
            'message' => 'Shop category has been successfully updated'
        ]);
    }

    public function deleteCoverageArea(ShopRequest $request, $shop_id)
    {
        \App\Models\Shop\CoverageArea::where('shop_id', $shop_id)
            ->where('master_state_id', $request->master_state_id)
            ->delete();

        return response([
            'message' => 'Shop coverage area has been successfully deleted.'
        ]);
    }

    public function updateCoverageArea(ShopRequest $request, $shop_id)
    {
        $cities = \App\Models\Master\City::whereIn('id', $request->master_city_ids)->get();

        $request->shop->coverage_areas()->where('master_state_id', $cities->first()->master_state_id)->delete();

        foreach($cities as $city)
        {
            \App\Models\Shop\CoverageArea::firstOrCreate([
                'shop_id' => $request->shop->id,
                'master_state_id' => $city->master_state_id,
                'master_city_id' => $city->id
            ]);
        }

        $request->shop->update([
            'is_coverage_area' => 1
        ]);

        return response([
            'message' => 'Shop coverage area has been successfully updated.'
        ]);
    }

    public function updateShopDetail(ShopRequest $request, $shop_id)
    {
        $user = \Auth::guard('api-user')->user();

        //@dnt_urgent_1 : check category and service id
        //@dnt_urgent_1 : upload image not yet done

        $request->shop->update([
            'name' => $request->shop_name,
        ]);

        if($request->shop_logo)
        {
            if($request->shop->shop_logo_path)
            {
                \App\Services\SaveFile::deleteFile($request->shop->shop_logo_path);
            }

            $shop_logo_path = \App\Services\SaveFile::saveFile('shop', $request->shop_logo);
            
            $request->shop->update([
                'shop_logo_path' => $shop_logo_path,
            ]);
        }

        if($request->delete_image_ids && !empty($request->delete_image_ids))
        {
            foreach($request->shop->images as $image)
            {
                if(in_array($image->id, $request->delete_image_ids))
                {
                    \App\Services\SaveFile::deleteFile($image->image_path);
                    $image->delete();
                }
            }
        }

        if($request->shop_images && !empty($request->shop_images))
        {
            foreach($request->shop_images as $shop_image)
            {
                if($shop_image)
                {
                    $photo_path = \App\Services\SaveFile::saveFile('shop', $shop_image);

                    if($photo_path)
                    {
                        $request->shop->images()->create([
                            'image_path' => $photo_path
                        ]);
                    }
                }

            }
        }

        $request->shop->detail()->updateOrCreate([
            'shop_id' => $request->shop->id
        ],[
            'address_1' => $request->address_1, 
            'address_2' => $request->address_2, 
            'address_3' => $request->address_3, 
            'master_state_id' => $request->master_state_id, 
            'master_city_id' => $request->master_city_id, 
            'postal_code' => $request->postal_code, 
            'tel_ext' => $request->tel_ext, 
            'tel_no' => $request->tel_no, 
            'hp_no' => $request->hp_no, 
            'business_email_address' => $request->business_email_address, 
            'pic' => $request->pic, 
        ]);

        $request->shop->update([
            'is_shop_detail' => 1
        ]);

        return response([
            'message' => 'Shop has been successfully updated'
        ]);
    }

    public function updateOperatingHour(ShopRequest $request, $shop_id)
    {
        $user = \Auth::guard('api-user')->user();

        $request->shop->operating_hours()->delete();

        foreach($request->operating_hours as $operating_hour)
        {
            if(!is_null($operating_hour['day']))
            {
                $operating_hour['shop_id'] = $request->shop->id;
                \App\Models\Shop\OperatingHour::create($operating_hour);
            }
        }

        $request->shop->update([
            'is_operating_hour' => 1
        ]);

        return response([
            'message' => 'Shop has been successfully updated'
        ]); 
    }

    public function updateBankAccount(ShopRequest $request, $shop_id)
    {
        $user = \Auth::guard('api-user')->user();

        \App\Models\Shop\Bank::updateOrCreate([
            'shop_id' => $request->shop->id
        ],[
            'master_bank_id' => $request->master_bank_id,
            'account_number' => $request->account_number,
            'beneficiary_name' => $request->beneficiary_name,
        ]);

        $request->shop->update([
            'is_business_account_detail' => 1
        ]);

        return response([
            'message' => 'Shop has been successfully updated'
        ]);
    }

    public function updateBookingConfirmation(ShopRequest $request, $shop_id)
    {
        $user = \Auth::guard('api-user')->user();

        $request->shop->update([
            'booking_method_id' => $request->book_method_id,
            'order_auto_reject_hour' => $request->book_method_id == 1 ? 0 : 24,
        ]);

       $request->shop->update([
            'is_booking_confirmation_detail' => 1
        ]);

        return response([
            'message' => 'Shop has been successfully updated'
        ]);
    }
    
    public function completed(ShopRequest $request, $shop_id)
    {
        \Auth::guard('api-user')->user()->profile->update([
            'is_shop_info_completed' => 1
        ]);

        $first_shop->users()->sync($user->id);

        return response([
            'message' => 'First shop has been completed'
        ]);
    }
}
