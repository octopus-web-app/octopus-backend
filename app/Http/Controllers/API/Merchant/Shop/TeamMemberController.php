<?php

namespace App\Http\Controllers\API\Merchant\Shop;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;
use App\Http\Requests\Merchant\Shop\ShopRequest; 
use App\Http\Resources\Merchant\TeamMemberResource; 
use App\Repositories\Shop\ShopRepo; 

class TeamMemberController extends Controller
{
    public function getTeamMembers(ShopRequest $request, $shop_id)
    {
        return TeamMemberResource::collection($request->shop->users()
            //->where('user_id', '!=', \Auth::guard('api-user')->user()->id)
            ->get());
    }

    public function addTeamMember(ShopRequest $request, $shop_id)
    {
        $mobile_no = \App\Services\Helper::standardize_mobile_phone($request->mobile_no);

        $user = \App\Models\User::firstOrCreate([
            'mobile_no' => $mobile_no
        ]);

        ShopRepo::firstOrCreateShopUser(
            $user->id,
            $shop_id
        );

        return response([
            'message' => 'Member has been successfully added.'
        ]);
    }
}
