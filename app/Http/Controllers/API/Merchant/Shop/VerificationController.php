<?php

namespace App\Http\Controllers\API\Merchant\Shop;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Http\Requests\Merchant\Shop\ShopRequest; 
use App\Repositories\Shop\VerificationRepo; 
use App\Http\Requests\Merchant\Shop\VerificationRequest; 

class VerificationController extends Controller
{
    public function getVerificationDocuments(Request $request)
    {
    	return \App\Models\Master\Verification\Document::get();
    }

    public function getVerificationPackages(ShopRequest $request, $shop_id)
    {
    	$verification_repo = VerificationRepo::createInstanceWithShop($request->shop);

		return $verification_repo->getVerificationPackages();
    }  

    public function getVerificationPackageDetail(ShopRequest $request, $shop_id, $master_verification_package_id)
    {
    	$verification_repo = VerificationRepo::createInstanceWithShop($request->shop);

		$verification_package = $verification_repo->getVerificationPackageDetail($master_verification_package_id);

		if($verification_package)
		{
			return $verification_package;
		}
		else
		{
			throw new \App\Exceptions\ApiAuthorizeException([
				'title' => 'Error',
				'message' => 'Invalid Selected Package.'
			]);
		}
    }  

    public function confirmVerificationPackage(ShopRequest $request, $shop_id, $master_verification_package_id)
    {
    	$verification_repo = VerificationRepo::createInstanceWithShop($request->shop);

		$verification_package = $verification_repo->getVerificationPackageDetail($master_verification_package_id);

		if(!$verification_package)
		{
			throw new \App\Exceptions\ApiAuthorizeException([
				'title' => 'Error',
				'message' => 'Invalid Selected Package.'
			]);
		}

		$verification_order_model = $verification_repo->confirmVerificationPackage($verification_package);

        return $verification_order_model;
    }

    public function uploadDocument(VerificationRequest $request)
    {
    	$master_verification_document = \App\Models\Master\Verification\Document::findOrFail($request->master_verification_document_id);

    	foreach($request->documents as $document)
    	{
        	$document_path = \App\Services\SaveFile::saveFile('shop_verification', $document);

        	\App\Models\Shop\Verification\Document::create([
        		'shop_id' => $request->shop->id,
		        'master_verification_document_id' => $master_verification_document->id, 
		        'document_path' => $document_path, 
        	]);
    	}
            
        return response([
            'message' => 'Shop verification document has been successfully upload'
        ]);
    }

    public function submitDocument(VerificationRequest $request)
    {
		$request->verification_order->update([
			'is_document_completed' => 1
		]);

        return response([
            'message' => 'Shop verification document has been successfully submitted'
        ]);
    }
}
 