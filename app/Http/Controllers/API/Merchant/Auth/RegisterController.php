<?php

namespace App\Http\Controllers\API\Merchant\Auth;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 

class RegisterController extends Controller
{
    public function register(RegistrationRequest $request)
    {
        $request->mobile_no = \App\Services\Helper::standardize_mobile_phone($request->mobile_no);

        $merchant_otp = \App\Models\Merchant\OTPCode::
            where('mobile_no', $request->mobile_no)
            ->where('otp_code', $request->otp_code)
            ->first();

        $request->customAuthorize($merchant_otp);

        $merchant = \App\Models\Merchant\Merchant::firstOrCreate([
            'mobile_no' => $request->mobile_no,
        ]);

        return response([
            'token' => $merchant->createToken('merchant-token')->plainTextToken
        ]);
    }

}