<?php

namespace App\Http\Controllers\API\Merchant\Auth;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Http\Requests\Merchant\Login as LoginRequest; 

class LoginController extends Controller
{
	public function login(LoginRequest $request)
	{
   		return response([
   			'token' => $request->user->createToken('my-app-token')->plainTextToken
   		]);
	}

	public function loginWithOTP(LoginRequest $request)
	{
   		return response([
   			'token' => $request->user->createToken('my-app-token')->plainTextToken
   		]);
	}
}
