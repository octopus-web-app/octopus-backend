<?php

namespace App\Http\Controllers\API\Merchant\Service;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Repositories\Service\ServiceRepo; 
use App\Http\Requests\Merchant\Shop\ShopRequest; 
use App\Http\Requests\Merchant\Service\ServiceRequest; 
use App\Http\Resources\Merchant\ServiceResource; 
use App\Repositories\Service\TeamRepo; 

class ServiceController extends Controller
{
    public function createService(ShopRequest $request, $shop_id)
    {
        $service = \App\Models\Service\Service::create([
            'shop_id' =>  $shop_id,
            'master_sub_category_id' => $request->master_sub_category_id,
            'name' => $request->name,
            'master_service_method_id' => $request->master_service_method_id,
        ]);

        return new ServiceResource($service);
    }

    public function getService(ServiceRequest $request)
    {
        return new ServiceResource($request->service);
    }

    public function getServices(ShopRequest $request, $shop_id)
    {
        return ServiceResource::collection($request->shop->services);
    }

    public function updateDetail(ServiceRequest $request, $service_id)
    {
        $request->service->update([
            'master_sub_category_id' => $request->master_sub_category_id,
            'name' => $request->name,
            'description' => $request->description,
            'master_service_method_id' => $request->master_service_method_id,
            'master_cancellation_policy_id' => $request->master_cancellation_policy_id,
            'base_price' => $request->base_price,
            'base_duration' => $request->base_duration,
            'warranty_duration_day' => $request->warranty_duration_day,
            'is_progressive_payment' => $request->is_progressive_payment,
            'delivery_lead_time_day' => $request->delivery_lead_time_day,
        ]);

        if($request->delete_image_ids && !empty($request->delete_image_ids))
        {
        	foreach($request->service->images as $image)
        	{
        		if(in_array($image->id, $request->delete_image_ids))
        		{
                    \App\Services\SaveFile::deleteFile($image->image_path);
        			$image->delete();
        		}
        	}
        }

        if($request->service_images && !empty($request->service_images))
        {
	        foreach($request->service_images as $service_image)
	        {
	        	if($service_image)
	        	{
            		$photo_path = \App\Services\SaveFile::saveFile('service_image', $service_image);

            		$request->service->images()->create([
            			'image_path' => $photo_path
            		]);
	        	}

	        }
        }

        return response([
            'message' => 'Service has been successfully updated'
        ]);
    }

    public function addTeamToService(ServiceRequest $request, $service_id)
    {   
        $shop_team_ids = $request->shop->teams->pluck('id')->toArray();

        $request->service->teams()->sync([]);

        foreach($request->team_ids as $team_id)
        {
            if(in_array($team_id, $shop_team_ids))
            {
                \App\Models\Service\ServiceTeam::create([
                    'service_id' => $service_id, 
                    'team_id' => $team_id, 
                ]);
            }
        }

        return response([
            'message' => 'Team has been successfully updated'
        ]);
    }

    public function getServiceTeams(ServiceRequest $request, $service_id)
    {   
        $team_repo = TeamRepo::createInstanceWithRequest($request);

        $service_team = $team_repo->getServiceTeam();

        return $service_team;
    }
}
