<?php

namespace App\Http\Controllers\API\Merchant\Service;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Repositories\Service\SKURepo; 
use App\Http\Requests\Merchant\Service\ServiceRequest; 
use App\Http\Resources\Merchant\SKUResource; 

class SKUController extends Controller
{
    public function generateSKU(ServiceRequest $request, $service_id)
    {
    	//\Log::info($request);
        //@dnt_urgent_1 : permission to update service option
    	$sku_repo = SKURepo::createInstanceWithService($request->service);

    	$sku_repo->generateSKU($request);  

        return response([
            'message' => 'Option has been successfully generated.'
        ]); 
    }

    public function getSKUs(ServiceRequest $request, $service_id)
    {
        $sku_repo = SKURepo::createInstanceWithService($request->service);

        return SKUResource::collection($sku_repo->getServiceSKUs());
    }

    public function getOptions(ServiceRequest $request, $service_id)
    {
        return $request->service->options()->select('name')->get();
    }

    public function updateSKUs(ServiceRequest $request, $service_id)
    {
        $sku_repo = SKURepo::createInstanceWithService($request->service);

        $sku_repo->updateSKUs($request);  

        return response([
            'message' => 'SKU has been successfully updated.'
        ]); 
    }

    public function addSKU(ServiceRequest $request, $service_id)
    {
        if(!$request->option_names)
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Failed',
                'message' => 'Invalid options data.'
            ]);
        }

        $service_option_models = $request->service->options;
        $number_of_options = $service_option_models->count();
        $request_option_names = array_filter($request->option_names);

        if($number_of_options != count($request_option_names))
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Failed',
                'message' => 'Invalid options data.'
            ]);

        }

        $sku_repo = SKURepo::createInstanceWithService($request->service);
        
        $sku_repo->addSKU($request);

        return response([
            'message' => 'SKU has been successfully added.'
        ]); 
    }
}
  