<?php

namespace App\Http\Requests\Merchant;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request; 

class LoginWithOTP extends FormRequest
{
    public function authorize()
    {
        $this->mobile_no = \App\Services\Helper::standardize_mobile_phone($this->mobile_no);

        if($this->route()->getName() == 'merchant.login.otp'){
        {
            $verify_otp = \App\Models\Core\OTPCode::
                where('mobile_no', $this->mobile_no)
                ->where('otp_code', $this->otp_code)
                ->first();

            if(!$verify_otp)
            {
                throw new \App\Exceptions\ApiAuthorizeException([
                    'title' => 'Login Failed',
                    'message' => 'Wrong Mobile Number or OTP Code.'
                ]);
            }

            $this->user = \App\Models\User::firstOrCreate([
                'mobile_no' =>  $this->mobile_no
            ],[]);
        }

        if(!$this->user)
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Login Failed',
                'message' => 'Wrong Mobile Number or OTP Code.'
            ]);
        }
        elseif($this->user->merchant_login_disabled)
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Login Failed',
                'message' => 'Your account has been suspended.'
            ]);
        }

        return true;
    }

    public function rules()
    {
        return [
            // 'mobile_no' => 'required',
        ];
    }

}
