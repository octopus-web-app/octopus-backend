<?php

namespace App\Http\Requests\Merchant;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request; 

class Profile extends FormRequest
{
    public function authorize()
    {
        if(\Auth::guard('api-user')->user()->profile && 
            \Auth::guard('api-user')->user()->profile->is_profile_completed)
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Error',
                'message' => 'This action is unauthorized.'
            ]);
        }

        return true;
    }

    public function rules()
    {
        if($this->route()->getName() == 'merchant.profile.update'){
            return [
                'name' => 'required|string',
                'email' => 'required|email|unique:users,email,'.\Auth::guard('api-user')->user()->id,
                'password' => 'required|string',
                'ic_no' => 'required|regex:/^[\w-]*$/|unique:users,ic_no,'.\Auth::guard('api-user')->user()->id,
            ];
        }
        elseif($this->route()->getName() == 'merchant.profile.upload'){
            return [
                'file_category' => 'required',
                'attachment' => 'required',
            ];
        }
        else
        {
            return [];
        }
    }

    public function uploadAuthorize()
    {

        if(
            !in_array($this->file_category, ['ic_front', 'ic_back', 'ic_selfie'])
        )
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Error',
                'message' => 'Incorrect File Category'
            ]);
        }

        if(!\Auth::guard('api-user')->user()->profile)
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Note',
                'message' => 'Please update profile before upload.'
            ]);
        }
    }
}
