<?php

namespace App\Http\Requests\Merchant\Service;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request; 

class ServiceRequest extends FormRequest
{
    public function authorize(Request $request)
    {
        $this->service = \App\Models\Service\Service::
            where('id', $request->service_id)
            ->with('shop')
            ->first();

        $this->shop = $this->service->shop;

        if(!$this->shop->users()->where('user_id', \Auth::guard('api-user')->user()->id)->exists())
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Error',
                'message' => 'Unauthorize Action.'
            ]);
        }

        return true;
    }

    public function rules()
    {
        return [
        ];
    }
}
