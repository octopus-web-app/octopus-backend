<?php

namespace App\Http\Requests\Merchant\Shop;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request; 

class VerificationRequest extends FormRequest
{
    public function authorize(Request $request)
    {
        $this->verification_order = \App\Models\Shop\Verification\Order::with('shop')->find($request->verification_order_id);
        $this->shop = $this->verification_order->shop;

        if(!$this->shop->users()->where('user_id', \Auth::guard('api-user')->user()->id)->exists())
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Error',
                'message' => 'Unauthorize Action.'
            ]);
        }

        return true;
    }

    public function rules()
    {
        return [
        ];
    }

    public function checkShopOwner($shop)
    {
        if(!$shop->merchants()->where('merchant_id', \Auth::guard('api-merchant')->user()->id)->exists())
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Error',
                'message' => 'Unauthorize Action.'
            ]);
        }
    }

    public function checkShopPendingVerificationExists($shop)
    {
        if($shop->pending_verification)
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Error',
                'message' => 'There are pending verification.'
            ]);
        }
    }

}
