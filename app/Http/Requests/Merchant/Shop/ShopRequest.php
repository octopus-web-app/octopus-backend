<?php

namespace App\Http\Requests\Merchant\Shop;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request; 
use App\Repositories\Shop\ShopRepo; 

class ShopRequest extends FormRequest
{
    public function authorize(Request $request)
    {
        $this->shop = \App\Models\Shop\Shop::find($request->shop_id);

        if(!$this->shop->users()->where('user_id', \Auth::guard('api-user')->user()->id)->exists())
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Error',
                'message' => 'Unauthorize Action.'
            ]);
        }

        return true;
    }

    public function rules()
    {
        return [
        ];
    }
}
