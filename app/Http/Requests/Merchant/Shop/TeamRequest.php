<?php

namespace App\Http\Requests\Merchant\Shop;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request; 

class TeamRequest extends FormRequest
{
    public function authorize(Request $request)
    {
        $this->team = \App\Models\Merchant\Team\Team::
            where('id', $request->team_id)
            ->with('shop')
            ->first();

        $this->shop = \App\Models\Shop\Shop::find($request->shop_id);

        if(!$this->shop->users()->where('user_id', \Auth::guard('api-user')->user()->id)->exists())
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Error',
                'message' => 'Unauthorize Action.'
            ]);
        }

        return true;
    }

    public function rules()
    {
        return [
        ];
    }
}
