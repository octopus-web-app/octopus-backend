<?php

namespace App\Http\Requests\Merchant;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request; 

class Login extends FormRequest
{
    public function authorize()
    {
        $this->mobile_no = \App\Services\Helper::standardize_mobile_phone($this->mobile_no);
        
        if($this->route()->getName() == 'merchant.login.otp')
        {
            $verify_otp = \App\Models\Core\OTPCode::
                where('mobile_no', $this->mobile_no)
                ->where('otp_code', $this->otp_code)
                ->first();

            if(!$verify_otp)
            {
                throw new \App\Exceptions\ApiAuthorizeException([
                    'title' => 'Login Failed',
                    'message' => 'Wrong Mobile Number or OTP Code.'
                ]);
            }

            $this->user = \App\Models\User::firstOrCreate([
                'mobile_no' =>  $this->mobile_no
            ],[]);
        }
        elseif($this->route()->getName() == 'merchant.login')
        {
            $this->user = \App\Models\User::
                where('mobile_no', $this->mobile_no)
                ->first();
        }
        else
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Login Failed',
                'message' => 'Invalid Route.'
            ]);
        }

        if(!$this->user)
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Login Failed',
                'message' => 'Invalid Credential'
            ]);
        }
        elseif($this->user->merchant_login_disabled)
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Login Failed',
                'message' => 'Your account has been suspended.'
            ]);
        }

        if($this->route()->getName() == 'merchant.login' && !\Hash::check($this->password, $this->user->password))
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Login Failed',
                'message' => 'Wrong email or password.'
            ]);
        }

        return true;
    }

    public function rules()
    {
        if($this->route()->getName() == 'merchant.login.otp')
        {
            return [
                'mobile_no' => 'required',
                'otp_code' => 'required',
            ];
        }
        else
        {
            return [
                'mobile_no' => 'required',
                'password' => 'required|string',
            ];
        }
    }

}
