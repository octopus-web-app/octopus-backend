<?php

namespace App\Http\Requests\Merchant;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request; 

class Registration extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'mobile_no' => 'required|unique:merchants|numeric',
            'otp_code' => 'required',
        ];
    }

    public function customAuthorize($merchant_otp)
    {
         if(!$merchant_otp)
        {
            throw new \App\Exceptions\ApiAuthorizeException([
                'title' => 'Failed',
                'message' => 'Wrong mobile number or OTP code.'
            ]);
        }
    }
}
