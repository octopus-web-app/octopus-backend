<?php

namespace App\Http\Middleware\Merchant;

use Closure;
use Illuminate\Http\Request;

class CheckMerchantGuard
{
    public function handle(Request $request, Closure $next, $guard = null)
    {
        if(\Auth::guard('api-user')->check() && !\Auth::guard('api-user')->user()->merchant_login_disabled)
        {
            return $next($request);
        }

        return response([
            'message' => ['This action does not authorize'] 
        ], 403);
    }
}
