<?php

namespace App\Http\Resources\Merchant;

use Illuminate\Http\Resources\Json\JsonResource;

class BankResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            // 'id' => $this->id,
            'master_bank_id' => $this->master_bank->id,
            'name' => $this->master_bank->name,
            'account_number' => $this->account_number,
            'beneficiary_name' => $this->beneficiary_name,
        ];
    }
}
