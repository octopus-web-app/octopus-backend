<?php

namespace App\Http\Resources\Merchant;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Merchant\BankResource; 
use App\Http\Resources\Merchant\ShopImageResource; 

class ShopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this ? $this->id : NULL,
            'name' => $this ? $this->name : NULL,
            'booking_method_id' => $this ? $this->booking_method_id : NULL,
            'master_category_id' => $this ? $this->master_category_id : NULL,
            'master_category_name' => isset($this->category->name) ?$this->category->name : NULL,
            'master_sub_categories' => $this && $this->sub_categories ? $this->sub_categories : [],
            'name' => $this ? $this->name : NULL,
            'description' => $this ? $this->description : NULL,
            'shop_logo_path' => $this->shop_logo_path ? url($this->shop_logo_path) : NULL,
            'address_1' => $this && $this->detail ? $this->detail->address_1 : NUll,
            'address_2' => $this && $this->detail ? $this->detail->address_2 : NUll,
            'address_3' => $this && $this->detail ? $this->detail->address_3 : NUll,
            'master_state_id' => $this && $this->detail ? $this->detail->master_state_id : NUll,
            'master_city_id' => $this && $this->detail ? $this->detail->master_city_id : NUll,
            'postal_code' => $this && $this->detail ? $this->detail->postal_code : NUll,
            'tel_ext' => $this && $this->detail ? $this->detail->tel_ext : NUll,
            'tel_no' => $this && $this->detail ? $this->detail->tel_no : NUll,
            'hp_no' => $this && $this->detail ? $this->detail->hp_no : NUll,
            'business_email_address' => $this && $this->detail ? $this->detail->business_email_address : NUll,
            'pic' => $this && $this->detail ? $this->detail->pic : NUll,
            'operating_hours' => $this ? $this->operating_hours : NULL,
            'banks' => $this ? BankResource::collection($this->banks) : NULL,
            'images' => $this ? ShopImageResource::collection($this->images) : NULL,
            'is_business_category' => $this ? $this->is_business_category : 0, 
            'is_shop_detail' => $this ? $this->is_shop_detail : 0, 
            'is_operating_hour' => $this ? $this->is_operating_hour : 0, 
            'is_business_account_detail' => $this ? $this->is_business_account_detail : 0, 
            'is_booking_confirmation_detail' => $this ? $this->is_booking_confirmation_detail : 0, 
            'is_shop_info_completed' => $this ? $this->is_shop_info_completed : 0, 
            'is_coverage_area' => $this ? $this->is_coverage_area : 0, 
        ];
    }
}
