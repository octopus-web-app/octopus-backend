<?php

namespace App\Http\Resources\Merchant;

use Illuminate\Http\Resources\Json\JsonResource;

class FirstShopStatusResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'is_business_category' => isset($this->is_business_category) ? $this->is_business_category : 0, 
            'is_shop_detail' => isset($this->is_shop_detail) ? $this->is_shop_detail : 0, 
            'is_operating_hour' => isset($this->is_operating_hour) ? $this->is_operating_hour : 0, 
            'is_business_account_detail' => isset($this->is_business_account_detail) ? $this->is_business_account_detail : 0, 
            'is_booking_confirmation_detail' => isset($this->is_booking_confirmation_detail) ? $this->is_booking_confirmation_detail : 0, 
            'is_first_shop_completed' => isset($this->is_first_shop_completed) ? $this->is_first_shop_completed : 0,
            'is_first_shop_exist' => \Auth::guard('api-user')->user()->shops()->exists() ? 1 : 0, 
        ];
    }
}
