<?php

namespace App\Http\Resources\Merchant;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Merchant\ServiceImageResource; 

class ServiceResource extends JsonResource
{
    public function toArray($request)
    {
        if(is_null($this->master_cancellation_policy_id))
        {
            $cancellation_policies = \App\Models\Master\CancellationPolicy::
                where('is_enable', 1)
                ->selectRaw('id, name, 0 as is_selected')
                ->get();
        }
        else
        {
            $cancellation_policies = \App\Models\Master\CancellationPolicy::
                where('is_enable', 1)
                ->selectRaw(
                    'id, name,
                    CASE
                        WHEN id IN ('.$this->master_cancellation_policy_id.') THEN 1
                        ELSE 0
                    END as is_selected'
                )->get();
        }

        return [
            'id' => $this ? $this->id : NULL,
            'master_sub_category_id' => $this ? $this->master_sub_category_id : NULL,
            'master_sub_category_name' => isset($this->sub_category->name) ? $this->sub_category->name : NULL,
            'master_service_method_id'=> $this ? $this->master_service_method_id : NULL,
            'master_service_method_name' => isset($this->service_method->name) ? $this->service_method->name : NULL,
            'name' => $this ? $this->name : NULL,
            'description' => $this ? $this->description : NULL,
            'base_price' => $this ? $this->base_price : NULL,
            'base_duration' => $this ? $this->base_duration : NULL,
            'warranty_duration_day' => $this ? $this->warranty_duration_day : NULL,
            'images' => $this ? ServiceImageResource::collection($this->images) : NULL,
            'cancellation_policy' => $cancellation_policies,
	    'delivery_lead_time_day' => $this->delivery_lead_time_day,
        ];
    }
}
