<?php

namespace App\Http\Resources\Merchant;

use Illuminate\Http\Resources\Json\JsonResource;

class TeamResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
	        'qualification' => $this->qualification,
	        'experience_amount' => $this->experience_amount,
	        'experience_type' => $this->experience_type,
        ];
    }
}
