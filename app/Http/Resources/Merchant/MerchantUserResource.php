<?php

namespace App\Http\Resources\Merchant;

use Illuminate\Http\Resources\Json\JsonResource;

class MerchantUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'ic_no' => $this->ic_no,
            'email' => $this->email,
            'mobile_no' => $this->mobile_no,
            'gender' => $this->gender,
            'dob' => $this->dob,
            'marital_status' => $this->marital_status,
            'ic_front' => isset($this->profile->ic_front) ? url($this->profile->ic_front) : NULL,
            'ic_back' => isset($this->profile->ic_back) ? url($this->profile->ic_back) : NULL,
            'ic_selfie' => isset($this->profile->ic_selfie) ? url($this->profile->ic_selfie) : NULL,
            'is_profile_completed' => isset($this->profile->is_profile_completed) ? $this->profile->is_profile_completed : 0,
        ];
    }
}
