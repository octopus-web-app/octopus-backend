<?php

namespace App\Http\Resources\Merchant;

use Illuminate\Http\Resources\Json\JsonResource;

class SKUResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this ? $this->id : NULL,
            'option_1'  => $this ? $this->option_1 : NULL,
            'option_2' => $this ? $this->option_2 : NULL, 
            'option_3' => $this ? $this->option_3 : NULL, 
            'price' => $this ? $this->price : NULL, 
            'duration' => $this ? $this->duration : NULL, 
            'image_path' => $this && $this->image_path ? url($this->image_path) : NULL, 
        ];
    }
}
