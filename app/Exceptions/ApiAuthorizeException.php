<?php

namespace App\Exceptions;

use Exception;

class ApiAuthorizeException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */

    public function __construct($reponse, $code = 444)
    {
        $this->reponse = $reponse;
        $this->code = $code;
    }

    public function report()
    {

    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response($this->reponse, $this->code);
    }
}