<?php


namespace App\Services;

class Helper
{
	public static function standardize_mobile_phone($mobile_no)
	{
		if(!$mobile_no)
		{
			return null;
		}
		if($mobile_no == '')
		{
			return null;
		}

		$mobile_no = str_replace(['+', '-'], '', filter_var($mobile_no, FILTER_SANITIZE_NUMBER_INT));

		if(substr($mobile_no,0,1) == '6')
		{
			$mobile_no = substr($mobile_no,1);
		}

		return $mobile_no;
    }
}
