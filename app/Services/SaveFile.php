<?php


namespace App\Services;

class SaveFile
{
	public static function saveFile($file_path, $file)
	{
		if($file)
		{
	        $fileName = date('Ymd')."_".uniqid().'.'.$file->getClientOriginalExtension();

	        $uploadpath = public_path().'/'.$file_path;

	        \File::put($uploadpath.'/'.$fileName, file_get_contents($file));

			return '/'.$file_path.'/'.$fileName;
		}

		return NULL;
	}

	public static function deleteFile($file_path)
	{
        \File::delete(public_path().$file_path);
	}
}
