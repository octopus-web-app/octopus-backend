<?php


namespace App\Services;

class MakePayment
{
	public static function paymentDefaultEntity($contents)
	{
        $contents['route'] = 'https://payment.ipay88.com.my/ePayment/entry.asp';
        
        $contents['entities'] = [
            'MerchantCode' => env('iPay_MerchantCode'),
            'PaymentId' => $payment_setting->payment_id,
            'RefNo' => $ref_no,
            'Amount' => $request->amount,
            'Currency' => 'MYR',
            'ProdDesc' => $payment_setting->payment_description,
            'UserName' => 'Ben',
            'UserEmail' => 'iambenghooi93@gmail.com',
            'UserContact' => '0123456789',
            'Signature' => 'lpbyOxnZcPidlOCRLRyVJKtNTuc=',
            'Remark' => '',
            'Lang' => 'UTF-8',
            'ResponseURL' => 'https://staging.lameway.com/response.php',
            'BackendURL' => 'https://staging.lameway.com/backend.php',
        ];

	}

	public function voidPreAuthTrans($version,$merchantCode,$transId,$currency,$amount,$signature)
	{
		$curl = curl_init();

		$xml = '<?xml version="1.0" encoding="utf-8"?>
		<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
		<soap12:Body>
		<VoidAuth xmlns="https://payment.ipay88.com.my/epayment/webservice/Transaction20/">
		<Version>'.$version.'</Version>
		<MerchantCode>'.$merchantCode.'</MerchantCode>
		<TransId>'.$transId.'</TransId>
		<Currency>'.$currency.'</Currency>
		<Amount>'.$amount.'</Amount>
		<Signature>'.$signature.'</Signature>
		</VoidAuth>
		</soap12:Body>
		</soap12:Envelope>';

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://payment.ipay88.com.my/ePayment/WebService/Transaction/VoidAuth.asmx',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS =>$xml,
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/soap+xml; charset=utf-8'
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);

		$your_xml_response = $response;
		$clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $your_xml_response);
		$xml = simplexml_load_string($clean_xml);

		$array = json_decode(json_encode((array)$xml), TRUE);

		return $array;
	}


	// capture pre-authed payment.
	public function capturePreAuthTrans($version,$merchantCode,$transId,$currency,$amount,$signature)
	{
		$curl = curl_init();
		$xml = '<?xml version="1.0" encoding="utf-8"?>
		<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
		<soap12:Body>
		<Capture xmlns="https://payment.ipay88.com.my/epayment/webservice/Transaction20/">
		<Version>'.$version.'</Version>
		<MerchantCode>'.$merchantCode.'</MerchantCode>
		<TransId>'.$transId.'</TransId>
		<Currency>'.$currency.'</Currency>
		<Amount>'.$amount.'</Amount>
		<Signature>'.$signature.'</Signature>
		</Capture>
		</soap12:Body>
		</soap12:Envelope>
		';

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://www.mobile88.com/ePayment/WebService/Transaction/capture.asmx',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => $xml,
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/soap+xml; charset=utf-8'
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);

		$your_xml_response = $response;
		$clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $your_xml_response);
		$xml = simplexml_load_string($clean_xml);

		$array = json_decode(json_encode((array)$xml), TRUE);

		return $array;
	}

	public function getSignature($ref_no, $amount)    
	{          
		$merchant_key = env('iPay_MerchantKey');
		$merchant_code = env('iPay_MerchantCode');
        $currency = 'MYR';

        $amount = str_replace(".","", $amount);
        $amount = str_replace(",","",$amount);

        $hash_string = $merchant_key.$merchant_code.$ref_no.$amount.$currency;       

        $signature = $this->iPay88Signature($hash_string);

        return $signature;
    }

	// generate Signature - request
	public function getPreauthVoidCaptureRequestSignature($preauthLog)    
	{          
		$MerchantKey = env('iPay_MerchantKey');
		$MerchantCode = env('iPay_MerchantCode');
		$TransId = $preauthLog->transid;
        $Amount = $preauthLog->amount; //'1.00';
        $Currency = 'MYR';

        $Amount_h = str_replace(".","", $Amount);
        $Amount_h = str_replace(",","",$Amount_h);
        $Amount_h = str_replace(",","",$Amount_h);
        $Amount_h = str_replace(",","",$Amount_h);      

        $hash_string = $MerchantKey.$MerchantCode.$TransId.$Amount_h.$Currency;        

        $mySinagure = $this->iPay88Signature($hash_string);

        return $mySinagure;
    }


	// generate Signature - Response
    public function getPreauthResponseSignature($responseArray)    
    {          
    	$MerchantKey = env('iPay_MerchantKey');
    	$MerchantCode = env('iPay_MerchantCode');
    	$TransId = $responseArray['Body']['VoidAuthResponse']['VoidAuthResult']['TransId'];
    	$Amount = $responseArray['Body']['VoidAuthResponse']['VoidAuthResult']['Amount'];
    	$Currency = $responseArray['Body']['VoidAuthResponse']['VoidAuthResult']['Currency'];
    	$Status =$responseArray['Body']['VoidAuthResponse']['VoidAuthResult']['Status'];

    	$Amount_h = str_replace(".","", $Amount);
    	$Amount_h = str_replace(",","",$Amount_h);
    	$Amount_h = str_replace(",","",$Amount_h);
    	$Amount_h = str_replace(",","",$Amount_h);

    	$hash_string = $MerchantKey.$MerchantCode.$TransId.$Amount_h.$Currency.$Status;

    	$mySinagure = $this->iPay88Signature($hash_string);
    	return $mySinagure;
    }

    public function iPay88Signature($source)
    {
    	return base64_encode(hex2bin(sha1($source)));
    }

    public function hex2bin($hexSource)
    {
    	for ($i=0;$i<strlen($hexSource);$i=$i+2)
    	{
    		$bin .= chr(hexdec(substr($hexSource,$i,2)));
    	}
    	return $bin;
    }

    public function savePaymentLog($request, $modal_id, $column_name)
    {
    	$entity = $request->all();
        $entity[$column_name] = $modal_id;

    	\App\Models\Log\Payment::create($entity);
    }
}
