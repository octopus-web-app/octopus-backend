<?php

namespace App\Imports;

use App\Models\Order;
use App\Models\UserConfig;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Auth;

use App\Repositories\UserConfigRepository;

class OrdersImport implements ToModel, WithHeadingRow
{
    private $rows = 0;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */


    public function model(array $row)
    {   
        ++$this->rows;

        $configs = UserConfig::where('xgroup','order_cost')->where('user_id',Auth::guard()->user()->id)->get();

        $xbroker_rate_percent = $this->getConfig($configs, 'xbroker_rate')/100;
        $xbroker_rate_min_buysales_amt = $this->getConfig($configs, 'xbroker_rate_min_amt');
        $xbroker_rate_sst_percent = $this->getConfig($configs, 'xbroker_rate_sst')/100;
        $xstamp_duty_rate_rm = $this->getConfig($configs, 'xstamp_duty_rate_rm');
        $xstamp_duty_rate_roundup_nearest = $this->getConfig($configs, 'xstamp_duty_rate_roundup_nearest');
        $xclearing_fee_rate_percent = $this->getConfig($configs, 'xclearing_fee_rate')/100;
        $xbroker_min_amt = $this->getConfig($configs, 'xbroker_min_amt');

        //Broker Fee
        //=IF(MatchVal<=xbroker_rate_min_buysales_amt,xbroker_min_amt,MatchVal*xbroker_rate)
        $broker_fee = ( $row['matchval'] <= $xbroker_rate_min_buysales_amt ) ? $xbroker_min_amt : $row['matchval'] * $xbroker_rate_percent ;

        //Broker Fee SST
        //=Broker Fee*Broker Rate SST

        $broker_fee_sst = $broker_fee * $xbroker_rate_sst_percent;

        // Clearing Fee
        // =MatchVal*Clearing Fee Rate
        $clearing_fee = $row['matchval'] * $xclearing_fee_rate_percent;

        // Stam Duty
        // =CEILING(MatchVal, Stamp Duty Rate roundup nearest)/Stamp Duty Rate roundup nearest*Stamp Duty Rate (RM)
        $stamp_duty = ( $this->roundUpToNearestMultiple($row['matchval'], $xstamp_duty_rate_roundup_nearest) ) / $xstamp_duty_rate_roundup_nearest * $xstamp_duty_rate_rm;

        $total_fee = $broker_fee + $broker_fee_sst + $clearing_fee + $stamp_duty;
        $total_amount = $broker_fee + $broker_fee_sst + $clearing_fee + $stamp_duty;
        return new Order([            
            'user_id'               => Auth::guard()->user()->id,
            'order_date'            => date('Y-m-d', strtotime($row['orddate'])),
            'order_time'            => date('h:i:s', strtotime($row['ordtime'])),
            'acc_no'                => $row['accno'],
            'order_no'              => $row['ordno'],
            'symbol'                => $row['symbol'],
            'action'                => $row['action'],
            'order_type'            => $row['ordtype'],
            'validity'              => $row['validity'],
            'status'                => $row['status'],
            'order_qty'             => $row['ordqty'],
            'order_price'           => $row['ordprice'],
            'total_match_qty'       => $row['totalmatchqty'],
            'avg_price'             => $row['avgprice'],
            'match_value'           => $row['matchval'],
            'settlement_currency'   => $row['settcurr'],
            'settlement_mode'       => $row['settmode'],
            'broker_fee'            => $broker_fee,
            'broker_fee_sst'        => $broker_fee_sst,
            'clearing_fee'          => $clearing_fee,
            'stamp_duty'            => $stamp_duty,
            'total_fee'             => $total_fee,
            'total_amount'          => $row['matchval'],
            'created_by'            => auth::guard()->user()->id,
            //'updated_by'            => $row['xxx'],
            'created_at'            => date('Y-m-d H:i:s'),
            //'updated_at'            => $row['xxx'],

        ]);
    }

    public function getRowCount(): int
    {
        return $this->rows;
    }

    private function getConfig($configs, $xkey)
    {
        foreach ($configs as $key => $value) {
           if( $value->xkey === $xkey )
           {
             return  $value->xvalue;
           }
        }
    }

    private function roundUpToNearestMultiple($n, $increment)
    {
        return (int) ($increment * ceil($n / $increment));
    }
}
