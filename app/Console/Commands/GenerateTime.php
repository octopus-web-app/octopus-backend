<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenerateTime extends Command
{
    protected $signature = 'generate:times';

    protected $description = 'Generate times';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $start_time = strtotime('00:00:00');

        while($start_time < strtotime('23:59:59'))
        {
            \App\Models\Master\Time::updateOrCreate([
                'time' => date("H:i:s", $start_time)
            ]);

            $start_time = strtotime("+15 minutes", $start_time);
        }
    }
}
