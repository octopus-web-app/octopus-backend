<?php

namespace App\Models\Merchant;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'merchant_profiles';

    protected $fillable = [
        'user_id',
        'ic_front',
        'ic_back',
        'ic_selfie',
        'is_profile_completed', 
    ];
}
