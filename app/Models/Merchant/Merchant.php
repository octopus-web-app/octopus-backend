<?php

namespace App\Models\Merchant;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Merchant extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'name', 
        'email', 
        'password',
        'ic_no',
        'mobile_no',
        'profile_photo_path',
        'status'
    ];
    
    public function sendPasswordResetNotification($token)
    {
    	$this->notify(new ResetPasswordNotification($token, 'merchants'));
    }

    public function shops()
    {
        return $this->belongsToMany('App\Models\Shop\Shop', 'shop_merchant', 'merchant_id', 'shop_id');
    }
    
}
