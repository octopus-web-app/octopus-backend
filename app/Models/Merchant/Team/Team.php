<?php

namespace App\Models\Merchant\Team;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';

    protected $fillable = [
        'shop_id',
        'name',
        'qualification',
        'experience_amount',
        'experience_type',
    ];
    
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'team_user', 'team_id', 'user_id');
    }

    public function shop()
    {
        return $this->belongsTo('App\Models\Shop\Shop', 'shop_id', 'id');
    }
    
    public function operating_hours()
    {
        return $this->hasMany('App\Models\Merchant\Team\OperatingHour', 'team_id', 'id');
    }
}