<?php

namespace App\Models\Merchant\Team;

use Illuminate\Database\Eloquent\Model;

class OperatingHour extends Model
{
    protected $table = 'team_operating_hours';

    protected $fillable = [
        'team_id',
        'day',
        'start_time',
        'end_time',
    ];
}