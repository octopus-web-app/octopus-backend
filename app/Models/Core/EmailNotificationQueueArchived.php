<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class EmailNotificationQueueArchived extends Model
{
    protected $fillable = [
        'from_name', 
        'from_email', 
        'to_name', 
        'to_email', 
        'email_subject', 
        'email_body', 
        'email_type',
        'to_cc',
        'to_bcc',
        'email_template', 
        'is_sent', 
        'sent_date', 
        'sent_status', 
        'created_by', 
        'updated_by',
        'created_at',
        'updated_at'
    ];

    protected $table = 'email_notification_queues_archived';
}
