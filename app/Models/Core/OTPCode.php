<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class OTPCode extends Model
{
    protected $table = 'core_otp_code';

    protected $fillable = [
        'otp_code',
        'mobile_no', 
    ];
    
}
