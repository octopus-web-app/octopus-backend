<?php

namespace App\Models\Log;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'log_payments';

    protected $fillable = [
        'verification_order_id',
        "MerchantCode",
        "PaymentId",
        "RefNo",
        "Amount",
        "Discount",
        "Currency",
        "Remark",
        "TransId",
        "AuthCode",
        "Status",
        "ErrDesc", 
        "PayerEmail",
        "ExpiryDate",
        "Signature",
        "CCName",
        "CCNo",
        "S_bankname",
        "S_country",
        "TokenId",
        "BindCardErrDesc",
        "PANTrackNo",
        "ActionType",
        "BankMID",
        "PayChannel",
        "PaymentType",
        "OldTokenId",
        "TranDate",
        'ip_address',
        'created_by',
        'updated_by',
    ];
}
