<?php

namespace App\Models\Cart;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'user_carts';

    protected $fillable = [
        'user_id', 
        'service_id',
        'sku_id',
        'quantity',
        'booking_date',
        'booking_time',
    ];


    public function users()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function service()
    {
        return $this->belongsTo('App\Models\Service\Service', 'service_id', 'id');
    }

    public function getApiResponse()
    {
        // return [
        //     'id' => $this ? $this->id : NULL,
        //     'master_sub_category_id' => $this ? $this->master_sub_category_id : NULL,
        //     'master_service_method_id'=> $this ? $this->master_service_method_id : NULL,
        //     'name' => $this ? $this->name : NULL,
        //     'description' => $this ? $this->description : NULL,
        //     'base_price' => $this ? $this->base_price : NULL,
        //     'base_duration' => $this ? $this->base_duration : NULL,
        //     'warranty_duration_day' => $this ? $this->warranty_duration_day : NULL,
        //     'images' => $this ? $this->images() : NULL,
	    //     'delivery_lead_time_day' => $this->delivery_lead_time_day,
	    //     'skus' => $this->skus,
	    //     'options' => $this->options,
        // ];
    }
}
