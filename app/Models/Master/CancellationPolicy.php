<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class CancellationPolicy extends Model
{
    protected $table = 'master_cancellation_policies';

    protected $fillable = [
        'name', 
        'description', 
        'duration_day', 
        'is_enable'
    ];

    protected $hidden = ['is_enable', 'created_at', 'updated_at'];
}
