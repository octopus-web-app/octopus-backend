<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'master_banks';

    protected $fillable = [
        'name', 'is_enable'
    ];
}
