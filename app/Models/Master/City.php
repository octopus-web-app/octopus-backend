<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'master_cities';

    protected $fillable = [
    	'master_state_id',
        'name',
    ];

    protected $hidden = ['created_at', 'updated_at'];

}
