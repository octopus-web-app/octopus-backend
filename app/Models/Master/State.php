<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'master_states';

    protected $fillable = [
        'name'
    ];
    
    protected $hidden = ['created_at', 'updated_at'];

    public function cities()
    {
        return $this->hasMany('App\Models\Master\City', 'master_state_id', 'id');
    }
}
