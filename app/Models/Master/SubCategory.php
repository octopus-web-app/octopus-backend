<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'master_sub_categories';

    protected $fillable = [
        'group_id', 'name', 'is_enable'
    ];

    protected $hidden = ['pivot', 'is_enable', 'created_at', 'updated_at'];

    public function service_methods()
    {
        return $this->belongsToMany('App\Models\Master\ServiceMethod', 'master_service_method_sub_category', 'master_sub_category_id', 'master_service_method_id');
    }
}
