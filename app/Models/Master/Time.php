<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    protected $table = 'master_times';

    protected $fillable = [
        'time', 
        'is_enable'
    ];
}
