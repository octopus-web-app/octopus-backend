<?php

namespace App\Models\Master\Verification;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $table = 'master_verification_discounts';

    protected $fillable = [
        'name', 
        'year_interval', 
        'discount', 
        'unique_key', 
        'from', 
        'to', 
        'logic_sort', 
    ];
    
    protected $hidden = ['pivot', 'created_at', 'updated_at'];
}
  