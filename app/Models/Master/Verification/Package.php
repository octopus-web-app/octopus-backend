<?php

namespace App\Models\Master\Verification;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = 'master_verification_packages';

    protected $fillable = [
        'name', 
        'month_interval', 
        'price', 
    ];

    protected $hidden = ['pivot', 'created_at', 'updated_at'];

}
  