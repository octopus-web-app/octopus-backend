<?php

namespace App\Models\Master\Verification;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'master_verification_documents';

    protected $fillable = [
        'key', 
        'name', 
    ];
    
    protected $hidden = ['created_at', 'updated_at'];
}
  