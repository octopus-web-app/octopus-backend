<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class ServiceMethod extends Model
{
    protected $table = 'master_service_methods';

    protected $fillable = [
        'name', 'description', 'is_enable'
    ];

    protected $hidden = ['pivot', 'is_enable', 'created_at', 'updated_at'];
}
