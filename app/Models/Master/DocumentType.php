<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
    protected $table = 'master_document_types';

    protected $fillable = [
    	'core_document_setting_id',
    ];

    public function document_setting()
    {
        return $this->belongsTo('App\Models\Core\DocumentSetting', 'core_document_setting_id', 'id');
    }
    
}
