<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'master_categories';

    protected $fillable = [
        'group_id', 'name', 'is_enable'
    ];

    protected $hidden = ['is_enable', 'created_at', 'updated_at'];

    public function sub_categories()
    {
        return $this->belongsToMany('App\Models\Master\SubCategory', 'master_category_sub', 'master_category_id', 'master_sub_category_id');
    }

    public function verification_packages()
    {
        return $this->belongsToMany('App\Models\Master\Verification\Package', 'master_verification_package_category', 'master_category_id', 'master_package_id');
    }

    public function verification_discounts()
    {
        return $this->belongsToMany('App\Models\Master\Verification\Discount', 'master_verification_discount_category', 'master_category_id', 'master_discount_id');
    }
}
