<?php

namespace App\Models\Service\Option;

use Illuminate\Database\Eloquent\Model;

class Value extends Model
{
    protected $table = 'service_option_values';

    protected $fillable = [
        'service_id', 
        'service_option_id', 
        'key', 
        'name', 
        'is_enabled',
    ];

}
