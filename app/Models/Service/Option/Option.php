<?php

namespace App\Models\Service\Option;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $table = 'service_options';

    protected $fillable = [
        'service_id', 
        'key', 
        'name', 
        'is_enabled',
    ];

    public function values()
    {
        return $this->hasMany('App\Models\Service\Option\Value', 'service_option_id', 'id');
    }
}
