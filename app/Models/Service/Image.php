<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'service_images';

    protected $fillable = [
        'service_id', 
        'image_path', 
    ];

    public function service()
    {
        return $this->belongsTo('App\Models\Service\Service', 'service_id', 'id');
    }
    
}
