<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Model;

class ServiceTeam extends Model
{
    protected $table = 'service_team';

    protected $fillable = [
        'service_id', 
        'team_id', 
    ];
}
