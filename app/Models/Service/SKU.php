<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Model;

class SKU extends Model
{
    protected $table = 'service_skus';

    protected $fillable = [
        'service_id', 
        'image_path', 
        'option_1', 
        'option_2', 
        'option_3', 
        'service_option_1_value_id', 
        'service_option_2_value_id', 
        'service_option_3_value_id', 
        'price', 
        'duration', 
        'image_path',
        'is_enabled', 
    ];

    public function service()
    {
        return $this->belongsTo('App\Models\Service\Service', 'service_id', 'id');
    }
    
}
