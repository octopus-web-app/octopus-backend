<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';

    protected $fillable = [
        'shop_id', 
        'master_sub_category_id',
        'master_service_method_id',
        'master_cancellation_policy_id',
        'name',
        'description',
        'base_price',
        'base_duration',
        'warranty_duration_day',
        'is_progressive_payment',
        'delivery_lead_time_day',
        'is_enabled',
    ];

    public function sub_category()
    {
        return $this->belongsTo('App\Models\Master\SubCategory', 'master_sub_category_id', 'id');
    }

    public function service_method()
    {
        return $this->belongsTo('App\Models\Master\ServiceMethod', 'master_service_method_id', 'id');
    }

    public function options()
    {
        return $this->hasMany('App\Models\Service\Option\Option', 'service_id', 'id');
    }

    public function shop()
    {
        return $this->belongsTo('App\Models\Shop\Shop', 'shop_id', 'id');
    }

    public function skus()
    {
        return $this->hasMany('App\Models\Service\SKU', 'service_id', 'id');
    } 

    public function images()
    {
        return $this->hasMany('App\Models\Service\Image', 'service_id', 'id');
    }

    public function teams()
    {
        return $this->belongsToMany('App\Models\Merchant\Team\Team', 'service_team', 'service_id', 'team_id');
    }

    public function carts()
    {
        return $this->hasMany('App\Models\Cart\Cart', 'service_id', 'id');
    }

    public function getApiResponse()
    {
        return [
            'id' => $this ? $this->id : NULL,
            'master_sub_category_id' => $this ? $this->master_sub_category_id : NULL,
            'master_service_method_id'=> $this ? $this->master_service_method_id : NULL,
            'name' => $this ? $this->name : NULL,
            'description' => $this ? $this->description : NULL,
            'base_price' => $this ? $this->base_price : NULL,
            'base_duration' => $this ? $this->base_duration : NULL,
            'warranty_duration_day' => $this ? $this->warranty_duration_day : NULL,
            'images' => $this ? $this->images() : NULL,
	        'delivery_lead_time_day' => $this->delivery_lead_time_day,
	        'skus' => $this->skus,
	        'options' => $this->options,
        ];
    }
}
