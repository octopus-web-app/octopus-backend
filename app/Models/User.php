<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Jetstream\HasTeams;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use HasTeams;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'ic_no', 
        'mobile_no',
        'gender',
        'dob',
        'marital_status',
        'profile_photo_path',
        'email_verified_at',
        'password',
        'merchant_login_disabled',
        'customer_login_disabled',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    public function profile()
    {
        return $this->hasOne('App\Models\Merchant\Profile', 'user_id', 'id');
    }

    public function shops()
    {
        return $this->belongsToMany('App\Models\Shop\Shop', 'shop_merchant_user', 'user_id', 'shop_id');
    }
    
    public function first_shop()
    {
        return $this->hasOne('App\Models\Shop\Shop', 'owner_user_id', 'id');
    }

    public function getApiResponse()
    {
        return [
            'name' => $this->name, 
            'email' => $this->email, 
            'ic_no' => $this->ic_no, 
            'mobile_no' => $this->mobile_no,
            'gender' => $this->gender,
            'dob' => $this->dob,
            'marital_status' => $this->marital_status,
            'profile_photo_path' => $this->profile_photo_path,
            'email_verified_at' => $this->email_verified_at,
            'merchant_login_disabled' => $this->merchant_login_disabled,
            'customer_login_disabled' => $this->customer_login_disabled
           
        ];
    }
    
}