<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $table = 'shops';

    protected $fillable = [
        'owner_user_id', 
        'master_category_id',  
        'booking_method_id',
        'order_auto_reject_hour',
        'name',
        'shop_logo_path', 
        'is_business_category', 
        'is_shop_detail', 
        'is_operating_hour', 
        'is_business_account_detail', 
        'is_booking_confirmation_detail', 
        'is_shop_info_completed', 
        'is_coverage_area',
        'status',
        'verified_from',
        'verified_to',
        'verified_interval',
    ];

    public function coverage_areas()
    {
        return $this->hasMany('App\Models\Shop\CoverageArea', 'shop_id', 'id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\Shop\Image', 'shop_id', 'id');
    }

    public function teams()
    {
        return $this->hasMany('App\Models\Merchant\Team\Team', 'shop_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'shop_merchant_user', 'shop_id', 'user_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Master\Category', 'master_category_id', 'id');
    }
    
    public function sub_categories()
    {
        return $this->belongsToMany('App\Models\Master\SubCategory', 'shop_sub_categories', 'shop_id', 'master_sub_category_id');
    }

    public function banks()
    {
        return $this->hasMany('App\Models\Shop\Bank', 'shop_id', 'id');
    }

    public function services()
    {
        return $this->hasMany('App\Models\Service\Service', 'shop_id', 'id');
    }

    public function detail()
    {
        return $this->hasOne('App\Models\Shop\Detail', 'shop_id', 'id');
    }

    public function operating_hours()
    {
        return $this->hasMany('App\Models\Shop\OperatingHour', 'shop_id', 'id');
    }

    public function bank()
    {
        return $this->hasOne('App\Models\Shop\Bank', 'shop_id', 'id')->latest('id');
    }

    public function lastest_verification()
    {
        return $this->hasOne('App\Models\Shop\Verification', 'shop_id', 'id')
            ->where('status', 'A')
            ->latest('id');
    }

    public function pending_verification()
    {
        return $this->hasOne('App\Models\Shop\Verification', 'shop_id', 'id')
            ->where('status', '!=', 'A');
    }
}
