<?php

namespace App\Models\Shop\Verification;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'verification_orders';

    protected $fillable = [
        'shop_id', 
        'prefix', 
        'running_no', 
        'display_running_no', 
        'transaction_id',
        'month_interval', 
        'approved_at',
        'expired_at',
        'deposit_name',
        'deposit_amount',
        'fee',
        'fee_name',
        'master_verification_package_id',
        'discount',
        'discount_name',
        'master_verification_discount_id',
        'tax_name',
        'tax',
        'total',
        'status',
        'is_document_completed',
        'is_payment_completed',
    ];

    public function shop()
    {
        return $this->belongsTo('App\Models\Shop\Shop', 'shop_id', 'id');
    }

}
