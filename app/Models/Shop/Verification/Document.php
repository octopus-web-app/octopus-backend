<?php

namespace App\Models\Shop\Verification;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'verification_documents';

    protected $fillable = [
        'shop_id', 
        'master_verification_document_id', 
        'document_path', 
    ];

    public function shop()
    {
        return $this->belongsTo('App\Models\Shop\Shop', 'shop_id', 'id');
    }
}
