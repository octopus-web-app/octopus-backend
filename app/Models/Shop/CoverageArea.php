<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;

class CoverageArea extends Model
{
    protected $table = 'shop_coverage_areas';

    protected $fillable = [
        'shop_id', 
        'master_state_id', 
        'master_city_id', 
    ];

    public function state()
    {
        return $this->belongsTo('App\Models\Master\State', 'master_state_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\Master\City', 'master_city_id', 'id');
    }
}
