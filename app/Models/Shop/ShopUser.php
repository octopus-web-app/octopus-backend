<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;

class ShopUser extends Model
{
    protected $table = 'shop_merchant_user';

    protected $fillable = [
        'user_id', 
        'shop_id', 
    ];
}
