<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;

class OperatingHour extends Model
{
    protected $table = 'shop_operating_hours';

    protected $fillable = [
        'shop_id', 
        'day',
        'open_time',
        'close_time',
    ];

}
