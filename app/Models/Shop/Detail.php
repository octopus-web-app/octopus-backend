<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $table = 'shop_details';

    protected $fillable = [
        'shop_id', 
        'address_1', 
        'address_2', 
        'address_3', 
        'master_state_id', 
        'master_city_id', 
        'postal_code',  
        'tel_ext', 
        'tel_no', 
        'hp_no', 
        'business_email_address', 
        'pic',
    ];

    public function state()
    {
        return $this->belongsTo('App\Models\Master\State', 'master_state_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\Master\City', 'master_city_id', 'id');
    }
}
