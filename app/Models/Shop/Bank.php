<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'shop_banks';

    protected $fillable = [
        'shop_id', 
        'master_bank_id', 
        'account_number',
        'beneficiary_name',
    ];

    public function master_bank()
    {
        return $this->belongsTo('App\Models\Master\Bank', 'master_bank_id', 'id');
    }
}
