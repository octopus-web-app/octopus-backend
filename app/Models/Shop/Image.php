<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'shop_images';

    protected $fillable = [
        'shop_id', 
        'image_path', 
    ];

    public function service()
    {
        return $this->belongsTo('App\Models\Shop\Shop', 'shop_id', 'id');
    }
    
}
