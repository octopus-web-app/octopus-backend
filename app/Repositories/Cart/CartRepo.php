<?php

namespace App\Repositories\Cart;
use Auth;
use App\Models\Cart\Cart as CartModel;


class CartRepo {


	public function getCart()
	{
		return CartModel::where('user_id',Auth::guard('api-user')->user()->id)->get();
	}
	
	public function addToCart($request)
	{	

		$cart_model = CartModel::firstOrCreate([
			'user_id' => Auth::guard('api-user')->user()->id,
			'service_id' => $request->service_id,
			'sku_id' => $request->sku_id,		
			'booking_date' => $request->booking_date,
			'booking_time' => $request->booking_time,
		]);

		$cart = CartModel::where('id', $cart_model->id)			
			->update(['quantity' => $cart_model->quantity+1]);
	
		//return CartModel::where('user_id',Auth::guard('api-user')->user()->id)->get();
	}



	// public function getServicesBySubCategoryId($request, $master_sub_category_id)
	// {
	// 	return ServiceModel::where('master_sub_category_id',$master_sub_category_id)->get();
	// }
}
