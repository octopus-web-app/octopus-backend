<?php

namespace App\Repositories\User;

use App\Models\UserAddress as UserAddressModel;
use Auth;

class UserRepo {

	public function createAddress($request)
	{
		$entity = UserAddressModel::create([
							'user_id' => Auth::guard('api-user')->user()->id,
							'address' => $request->address,
							'latitude' => $request->latitude,
							'longitude' => $request->longitude, 
							'status ' => 1 
						]);

		return $entity;
	}

}
