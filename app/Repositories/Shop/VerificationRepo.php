<?php

namespace App\Repositories\Shop;

use App\Models\Master\Verification\Package as PackageModel;
use App\Models\Master\Verification\Discount as DiscountModel;
use App\Models\Shop\Verification\Order as VerificationOrderModel;

class VerificationRepo
{
	public static function createInstanceWithShop($shop)
	{
		$verification_repo = new VerificationRepo;

		$verification_repo->shop = $shop;
		
		return $verification_repo;
	}

	public function getVerificationPackages()
	{
		$deposit = 200;
		$discount_unique_keys = $this->getShopDiscountUniqueKeys();

		$verification_packages = $this->shop->category->verification_packages;

		$verification_discount = $this->shop->category->verification_discounts()
			->where(function ($query) {
				$query->where(function ($query) {
					$query->where('from', '<=', date('Y-m-d'));
					$query->where('to', '>=', date('Y-m-d'));
				})->orWhere('from', NULL);
			})
			->where(function ($query) use ($discount_unique_keys) {
				$query->whereNotIn('unique_key', $discount_unique_keys);
				$query->orWhereNull('unique_key');
			})
			->where('year_interval', '<=', $this->shop->verified_interval)
			->orderBy('logic_sort')
			->first();

		foreach($verification_packages as $verification_package)
		{
			$verification_package->deposit = $deposit;
			$verification_package->deposit_name = 'Verified Vendor Deposit';

			if($verification_discount)
			{
				$verification_package->discount_id = $verification_discount->id;
				$verification_package->discount = $verification_discount->discount;
				$verification_package->discount_name = $verification_discount->name;
				$verification_package->total = $deposit + $verification_package->price * (100 - $verification_discount->discount) / 100;
			}
			else
			{
				$verification_package->total = $deposit + $verification_discount->price;
			}
		}

		return $verification_packages;
	}

	public function getShopDiscountUniqueKeys()
	{
		return [];
	}

	public function getVerificationPackageDetail($master_verification_package_id)
	{
		$verification_packages = $this->getVerificationPackages();

		return $verification_packages->where('id', $master_verification_package_id)->first();
	}

	public function confirmVerificationPackage($verification_package)
	{
		$verification_order_running_no = VerificationOrderModel::max('running_no');
		
		$running_no = $verification_order_running_no ? $verification_order_running_no + 1 : 1;

		$entity = [
			'shop_id' => $this->shop->id,
			'prefix' => 'VV',
			'running_no' => $running_no,
			'display_running_no' => 'VV'.sprintf('%08d', $running_no),
			'month_interval' => $verification_package->month_interval,
			'total' => $verification_package->total,
	        'master_verification_package_id' => $verification_package->id, 
	        'name' => $verification_package->name, 
	        'amount' => $verification_package->price, 
		];


		if(isset($verification_package->deposit) && !is_null($verification_package->deposit))
		{
			$entity['deposit_name'] = $verification_package->deposit_name;
			$entity['deposit'] = $verification_package->deposit;
		}

		if(isset($verification_package->discount))
		{
			$entity['discount_name'] = $verification_package->discount_name;
			$entity['discount'] = $verification_package->discount;
			$entity['master_verification_discount_id'] = $verification_package->discount_id;
		}

		$verification_model = VerificationOrderModel::create($entity);

		return $verification_model;
	}
}