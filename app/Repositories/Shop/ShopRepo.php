<?php

namespace App\Repositories\Shop;

use App\Models\Shop\Shop as ShopModel;
use App\Models\Shop\ShopUser as ShopUserModel;

class ShopRepo
{
	public static function firstOrCreateShopUser($user_id, $shop_id)
	{
		return \App\Models\Shop\ShopUser::updateOrCreate([
			'user_id' => $user_id,
			'shop_id' => $shop_id,
		]);
	}

	public static function getCoverageAreas($shop)
	{
		$coverage_areas = $shop->coverage_areas()->with('city','state')->get();

		$unique_states = $coverage_areas->unique('state');

		$entities = collect([]);

		foreach($unique_states as $unique_state)
		{
			$entity['id'] = $unique_state->master_state_id;
			$entity['state_name'] = $unique_state->state->name;
			$entity['cities'] = $coverage_areas->where('master_state_id', $unique_state->master_state_id)->pluck('city');

			$entities->push($entity);
		}

		return $entities;
	}
}
