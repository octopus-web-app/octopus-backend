<?php

namespace App\Repositories\Shop;

use App\Models\Service\Option\Option as OptionModel;

class CategoryRepo
{
	public static function createInstanceWithServiceID($service_id)
	{
		$service = new Service;

		$service->service_id = $service_id;

		return $service;
	}
}
