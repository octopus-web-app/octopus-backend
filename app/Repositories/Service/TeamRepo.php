<?php

namespace App\Repositories\Service;

use App\Models\Merchant\Team\Team as TeamModel;
use App\Http\Resources\Merchant\TeamMemberResource; 

class TeamRepo {

	public static function createInstanceWithRequest($request)
	{
		$team_repo = new TeamRepo;

		$team_repo->service = $request->service;
		$team_repo->shop = $request->shop;

		return $team_repo;
	}

	public function getServiceTeam()
	{
		$service_team_ids = $this->service->teams()->select('team_id')->get()->pluck('team_id')->toArray();

		$shop_users = $this->shop->users->map->only(['id', 'name', 'mobile_no']);

		$service_teams = TeamModel::
			where('shop_id', $this->shop->id)
			->with('operating_hours')
			->with('users');

		if(empty($service_team_ids))
		{	
			$service_teams->selectRaw(
				'id, name, qualification, experience_amount, 0 as is_selected'
			);
		}
		else
		{

			$service_teams->selectRaw(
				'id, name, qualification, experience_amount,
				CASE
				    WHEN id IN ('.implode(",",$service_team_ids).') THEN 1
				    ELSE 0
				END
				as is_selected'
			);
		}

		$service_teams = $service_teams->get();

		foreach($service_teams as $service_team)
		{
			$service_team_users = $service_team->users->pluck('id')->toArray();
			unset($service_team['users']);

			$service_team->team_users = $shop_users->map(function ($user) use ($service_team_users) {

			    if(in_array($user['id'], $service_team_users))
			    {
			    	$user['is_selected'] = 1;
			    }
			    else
			    {
			    	$user['is_selected'] = 0;
			    }

			    return $user;
			});
		}


		return $service_teams;
	}
}
