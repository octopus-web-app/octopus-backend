<?php

namespace App\Repositories\Service;

use App\Models\Service\Option\Option as OptionModel;
use App\Models\Service\Option\Value as ValueModel;
use App\Models\Service\Service as ServiceModel;
use App\Models\Service\SKU as ServiceSKUModel;

class ServiceRepo {

	public static function createInstanceWithServiceID($service_id)
	{
		$service_repo = new ServiceRepo;

		$service_repo->service_id = $service_id;

		return $service_repo;
	}

	public function updateOptions($request)
	{
		OptionModel::
			where('service_id', $this->service_id)
			->delete();

		ValueModel::
			where('service_id', $this->service_id)
			->delete();

		ServiceSKUModel::
			where('service_id', $this->service_id)
			->update([
				'is_enabled' => 0
			]);


		foreach($request->options as $option)
		{
			if(isset($option['values']) && !is_null($option['values']) && isset($option['values']) && !empty($option['values']))
			{
				$option_model = OptionModel::create([
					'service_id' => $this->service_id,
					'name' => $option['name'],
					'is_enabled' => 1
				]);

				foreach($option['values'] as $value)
				{
					if($value)
					{
						ValueModel::create([
							'service_id' => $this->service_id,
							'service_option_id' => $option_model->id,
							'name' => $value,
							'is_enabled' => 1
						]);
					}
				}
			}
		}

		$options = OptionModel::
			where('service_id', $this->service_id)
			->with('values')
			->get();

		$option_1_values = isset($options[0]) ? $options[0]->values : [];
		$option_2_values = isset($options[1]) ? $options[1]->values : [];
		$option_3_values = isset($options[2]) ? $options[2]->values : [];

		foreach($option_1_values as $option_1_value)
		{
			if(empty($option_2_values))
			{
				ServiceSKUModel::updateOrCreate([
					'service_id' => $this->service_id,
					'option_1' => $option_1_value->name,
				],[
					'is_enabled' => 1
				]);
			}
			else
			{
				foreach($option_2_values as $option_2_value)
				{
					if(empty($option_3_values))
					{
						ServiceSKUModel::updateOrCreate([
							'service_id' => $this->service_id,
							'option_1' => $option_1_value->name,
							'option_2' => $option_2_value->name,
						],[
							'is_enabled' => 1
						]);
					}
					else
					{
						foreach($option_3_values as $option_3_value)
						{
							ServiceSKUModel::updateOrCreate([
								'service_id' => $this->service_id,
								'option_1' => $option_1_value->name,
								'option_2' => $option_2_value->name,
								'option_3' => $option_3_value->name
							],[
								'is_enabled' => 1
							]);
						}
					}
				}
			}
		}
	}

	public function updateServices($request)
	{
		ServiceSKUModel::
			where('service_id', $this->service_id)
			->update([
				'is_enabled' => 0
			]);

		foreach($request->options as $option)
		{
			ServiceSKUModel::updateOrCreate([
		        'service_id' => $this->service_id,
		        'option_1' => $option['option_1'], 
		        'option_2' => $option['option_2'], 
		        'option_3' => $option['option_3'], 
			],[
		        'price' => $option['price'], 
		        'duration' => $option['duration'], 。
			]);
		}
	}

	public function getServicesBySubCategoryId($request, $master_sub_category_id)
	{
		return ServiceModel::where('master_sub_category_id',$master_sub_category_id)->get();
	}
}
