<?php

namespace App\Repositories\Service;

use App\Models\Service\Option\Option as OptionModel;
use App\Models\Service\Option\Value as ValueModel;
use App\Models\Service\Service as ServiceModel;
use App\Models\Service\SKU as ServiceSKUModel;

class SKURepo {

	public static function createInstanceWithService($service)
	{
		$sku_repo = new SKURepo;

		$sku_repo->service = $service;

		return $sku_repo;
	}

	public function getServiceSKUs()
	{
		return $this->service->skus()->where('is_enabled', 1)->get();
	}

	public function updateSKUs($request)
	{
		$sku_ids_array = $this->service->skus->pluck('id')->toArray();

		foreach($request->skus as $sku)
		{
			if(isset($sku['id']) 
				&& isset($sku['price']) && !is_null($sku['price']) 
				&& isset($sku['duration']) && !is_null($sku['duration'])
				&& in_array($sku['id'], $sku_ids_array)
			){
				$sku_model = ServiceSKUModel::find($sku['id']);

				$entity = [
					'price' => $sku['price'] + $this->service->base_price,
					'duration' => $sku['duration'] + $this->service->base_duration,
				];

				if(isset($sku['image']) && !is_null($sku['image']))
				{
            		$photo_path = \App\Services\SaveFile::saveFile('sku_image', $sku['image']);

            		if($photo_path)
            		{
            			$entity['image_path'] = $photo_path;

            			if($sku_model->image_path)
            			{
                    		\App\Services\SaveFile::deleteFile($sku_model->image_path);
            			}
            		}
				}

				$sku_model->update($entity);
			}
		}
	}

	public function generateSKU($request)
	{
		OptionModel::
			where('service_id', $this->service->id)
			->delete();

		ValueModel::
			where('service_id', $this->service->id)
			->delete();

		ServiceSKUModel::
			where('service_id', $this->service->id)
			->update([
				'is_enabled' => 0
			]);

		foreach($request->options as $option)
		{
			if(isset($option['name']) && !is_null($option['name']) && isset($option['values']) && !empty($option['values']))
			{
				$option_model = OptionModel::create([
					'service_id' => $this->service->id,
					'name' => $option['name'],
					'is_enabled' => 1
				]);

				foreach($option['values'] as $value)
				{
					if($value)
					{
						ValueModel::create([
							'service_id' => $this->service->id,
							'service_option_id' => $option_model->id,
							'name' => $value,
							'is_enabled' => 1
						]);
					}
				}
			}
		}

		$options = OptionModel::
			where('service_id', $this->service->id)
			->with('values')
			->get();

		$option_1_values = isset($options[0]) ? $options[0]->values : [];
		$option_2_values = isset($options[1]) ? $options[1]->values : [];
		$option_3_values = isset($options[2]) ? $options[2]->values : [];

		foreach($option_1_values as $option_1_value)
		{
			if(empty($option_2_values))
			{
				ServiceSKUModel::updateOrCreate([
					'service_id' => $this->service->id,
					'option_1' => $option_1_value->name,
				],[
					'service_option_1_value_id' => $option_1_value->id,
					'is_enabled' => 1
				]);
			}
			else
			{
				foreach($option_2_values as $option_2_value)
				{
					if(empty($option_3_values))
					{
						ServiceSKUModel::updateOrCreate([
							'service_id' => $this->service->id,
							'option_1' => $option_1_value->name,
							'option_2' => $option_2_value->name,
						],[
							'service_option_1_value_id' => $option_1_value->id,
							'service_option_2_value_id' => $option_2_value->id,
							'is_enabled' => 1
						]);
					}
					else
					{
						foreach($option_3_values as $option_3_value)
						{
							ServiceSKUModel::updateOrCreate([
								'service_id' => $this->service->id,
								'option_1' => $option_1_value->name,
								'option_2' => $option_2_value->name,
								'option_3' => $option_3_value->name
							],[
								'service_option_1_value_id' => $option_1_value->id,
								'service_option_2_value_id' => $option_2_value->id,
								'service_option_3_value_id' => $option_3_value->id,
								'is_enabled' => 1
							]);
						}
					}
				}
			}
		}
	}

	public function addSKU($request)
	{
        foreach(array_filter($request->option_names) as $key => $request_option_name)
        {
            $service_option_model = $this->service->options[$key];

            $service_option_value_model = $service_option_model
                ->values()
                ->firstOrCreate([
                    'service_id' => $this->service->id,
                    'name' => $request_option_name,
                ]);

             $create_entity['service_id'] = $this->service->id;
             $create_entity['option_'.($key + 1)] = $request_option_name;
             $update_entity['service_option_'.($key + 1).'_value_id'] = $service_option_value_model->id;
             $update_entity['is_enabled'] = 1;
        }

        ServiceSKUModel::updateOrCreate($create_entity,$update_entity);
	}
}
