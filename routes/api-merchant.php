<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('profile')->group(function () { 
	Route::get('/', [\App\Http\Controllers\API\Merchant\Profile\ProfileController::class, 'getProfile'])->name('merchant.profile');

	Route::post('/', [\App\Http\Controllers\API\Merchant\Profile\ProfileController::class, 'update'])->name('merchant.profile.update');
	Route::post('/upload', [\App\Http\Controllers\API\Merchant\Profile\ProfileController::class, 'upload'])->name('merchant.profile.upload');
	Route::post('/completed', [\App\Http\Controllers\API\Merchant\Profile\ProfileController::class, 'completed'])->name('merchant.profile.completed');
});

// Route::prefix('first-shop')->group(function () { 

// 	Route::get('/status', [\App\Http\Controllers\API\Merchant\Shop\FirstShopController::class, 'getStatus'])->name('merchant.first-shop.status');
// 	Route::post('/update-business-category', [\App\Http\Controllers\API\Merchant\Shop\FirstShopController::class, 'updateBusinessCategory'])->name('merchant.first-shop.update-business-category');

// });

Route::prefix('shop')->group(function () { 

	Route::get('/first', [\App\Http\Controllers\API\Merchant\Shop\ShopController::class, 'getFirstShop'])->name('merchant.first-shop');
	Route::get('/first/status', [\App\Http\Controllers\API\Merchant\Shop\ShopController::class, 'getFirstShopStatus'])->name('merchant.first-shop');

	Route::post('/create', [\App\Http\Controllers\API\Merchant\Shop\ShopController::class, 'createShop'])->name('merchant.shop.create');

	Route::prefix('{shop_id}')->group(function () { 

		Route::get('/', [\App\Http\Controllers\API\Merchant\Shop\ShopController::class, 'getShop'])->name('merchant.shop');
		Route::get('/coverage-areas', [\App\Http\Controllers\API\Merchant\Shop\ShopController::class, 'getShopCoverageShop'])->name('merchant.shop.coverage-area');

		Route::post('/update-detail', [\App\Http\Controllers\API\Merchant\Shop\ShopController::class, 'updateShopDetail'])->name('merchant.shop.update-shop-detail');
		Route::post('/update-category', [\App\Http\Controllers\API\Merchant\Shop\ShopController::class, 'updateCategory'])->name('merchant.shop.update-shop-detail');
		Route::post('/update-coverage-area', [\App\Http\Controllers\API\Merchant\Shop\ShopController::class, 'updateCoverageArea'])->name('merchant.shop.update-coverage-area');
		Route::post('/delete-coverage-area', [\App\Http\Controllers\API\Merchant\Shop\ShopController::class, 'deleteCoverageArea'])->name('merchant.shop.delete-coverage-area');
		Route::post('/update-operating-hour', [\App\Http\Controllers\API\Merchant\Shop\ShopController::class, 'updateOperatingHour'])->name('merchant.shop.update-operating-hour');
		Route::post('/update-bank-account', [\App\Http\Controllers\API\Merchant\Shop\ShopController::class, 'updateBankAccount'])->name('merchant.shop.update-bank-account');
		Route::post('/update-booking-confirmation', [\App\Http\Controllers\API\Merchant\Shop\ShopController::class, 'updateBookingConfirmation'])->name('merchant.shop.update-booking-confirmation');
		
		Route::get('/services', [\App\Http\Controllers\API\Merchant\Service\ServiceController::class, 'getServices'])->name('merchant.services');


		Route::prefix('service')->group(function () { 
			Route::post('/create', [\App\Http\Controllers\API\Merchant\Service\ServiceController::class, 'createService'])->name('merchant.service.create');
		});

		Route::get('/teams', [\App\Http\Controllers\API\Merchant\Shop\TeamController::class, 'getTeams'])->name('merchant.teams');

		Route::prefix('team')->group(function () { 
			Route::post('/create', [\App\Http\Controllers\API\Merchant\Shop\TeamController::class, 'createTeam'])->name('merchant.team.create');
		});

		Route::prefix('sub-category')->group(function () { 
			Route::get('/selected', [\App\Http\Controllers\API\Merchant\Shop\CategoryController::class, 'getSelectedSubCatogory'])->name('merchant.shop.sub-category.selected');
		});

		Route::post('/update', [\App\Http\Controllers\API\Merchant\Shop\ShopController::class, 'update'])->name('merchant.shop.update');
		Route::post('/update-payment-method', [\App\Http\Controllers\API\Merchant\Shop\ShopController::class, 'updatePaymentMethod'])->name('merchant.shop.update-payment-method');
		Route::delete('/', [\App\Http\Controllers\API\Merchant\Shop\ShopController::class, 'delete'])->name('merchant.shop.delete');

		Route::prefix('team-members')->group(function () { 

			Route::get('', [\App\Http\Controllers\API\Merchant\Shop\TeamMemberController::class, 'getTeamMembers'])->name('merchant.shop.team-member');

		});

		Route::prefix('team-member')->group(function () { 

			Route::get('', [\App\Http\Controllers\API\Merchant\Shop\TeamMemberController::class, 'getTeamMember'])->name('merchant.shop.team-member');
			Route::post('/add', [\App\Http\Controllers\API\Merchant\Shop\TeamMemberController::class, 'addTeamMember'])->name('merchant.shop.team-member.add');

		});

		Route::prefix('verification')->group(function () { 

			Route::get('/packages', [\App\Http\Controllers\API\Merchant\Shop\VerificationController::class, 'getVerificationPackages'])->name('merchant.shop.verification.packages');
			Route::get('/package/{master_verification_package_id}', [\App\Http\Controllers\API\Merchant\Shop\VerificationController::class, 'getVerificationPackageDetail'])->name('merchant.shop.verification.package');
			Route::get('/package/{master_verification_package_id}/confirm', [\App\Http\Controllers\API\Merchant\Shop\VerificationController::class, 'confirmVerificationPackage'])->name('merchant.shop.verification.package.confirm');

			Route::get('/pending', [\App\Http\Controllers\API\Merchant\Shop\VerificationController::class, 'getPending'])->name('merchant.shop.verification.pending');
			Route::post('/create', [\App\Http\Controllers\API\Merchant\Shop\VerificationController::class, 'create'])->name('merchant.shop.verification.create');
			Route::post('/{shop_verification_id}/update', [\App\Http\Controllers\API\Merchant\Shop\VerificationController::class, 'update'])->name('merchant.shop.verification.update');
			Route::post('/{shop_verification_id}/upload', [\App\Http\Controllers\API\Merchant\Shop\VerificationController::class, 'uploadDocument'])->name('merchant.shop.verification.upload');
		});

	});

});

Route::prefix('team')->group(function () { 

	Route::prefix('{team_id}')->group(function () { 

		Route::get('/', [\App\Http\Controllers\API\Merchant\Shop\TeamController::class, 'getTeam'])->name('merchant.team');

	});
});

Route::prefix('verification')->group(function () { 

	Route::post('/{verification_order_id}/upload', [\App\Http\Controllers\API\Merchant\Shop\VerificationController::class, 'uploadDocument'])->name('merchant.shop.verification.upload');
	Route::get('/{verification_order_id}/submit', [\App\Http\Controllers\API\Merchant\Shop\VerificationController::class, 'submitDocument'])->name('merchant.shop.verification.submit');

});

Route::prefix('service')->group(function () { 

	Route::post('/', [\App\Http\Controllers\API\Merchant\Service\ServiceController::class, 'createService'])->name('merchant.service.create');

	Route::prefix('{service_id}')->group(function () { 

		Route::get('/', [\App\Http\Controllers\API\Merchant\Service\ServiceController::class, 'getService'])->name('merchant.service');

		Route::post('/update-detail', [\App\Http\Controllers\API\Merchant\Service\ServiceController::class, 'updateDetail'])->name('merchant.service.update-detail');

		Route::get('/options', [\App\Http\Controllers\API\Merchant\Service\SKUController::class, 'getOptions'])->name('merchant.service.sku.options');

		Route::prefix('sku')->group(function () { 

			Route::get('/', [\App\Http\Controllers\API\Merchant\Service\SKUController::class, 'getSKUs'])->name('merchant.service.sku.store');

			Route::post('/generate', [\App\Http\Controllers\API\Merchant\Service\SKUController::class, 'generateSKU'])->name('merchant.service.sku.generate');

			Route::post('/update', [\App\Http\Controllers\API\Merchant\Service\SKUController::class, 'updateSKUs'])->name('merchant.service.sku.update');

			Route::post('/add', [\App\Http\Controllers\API\Merchant\Service\SKUController::class, 'addSKU'])->name('merchant.service.sku.add');

		});

		Route::post('team', [\App\Http\Controllers\API\Merchant\Service\ServiceController::class, 'addTeamToService'])->name('merchant.service.team.add');
		Route::get('teams', [\App\Http\Controllers\API\Merchant\Service\ServiceController::class, 'getServiceTeams'])->name('merchant.service.teams');

	});

});