<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('user')->group(function () { 
	Route::post('/login', [\App\Http\Controllers\API\User\Auth\LoginController::class, 'login'])->name('user.login');
	Route::post('/register', [\App\Http\Controllers\API\User\Auth\RegisterAuthController::class, 'register'])->name('user.register');
});

Route::prefix('merchant')->group(function () { 

	Route::post('/send-otp', [\App\Http\Controllers\API\OTP\OTPController::class, 'sendOTP'])->name('merchant.otp');
	Route::post('/login-with-otp', [\App\Http\Controllers\API\Merchant\Auth\LoginController::class, 'loginWithOTP'])->name('merchant.login.otp');

	Route::post('/login', [\App\Http\Controllers\API\Merchant\Auth\LoginController::class, 'login'])->name('merchant.login');
	// Route::post('/register', [\App\Http\Controllers\API\Merchant\Auth\RegisterController::class, 'register'])->name('merchant.register');
    Route::post('/forgot-password', [\App\Http\Controllers\API\Merchant\Auth\PasswordResetLinkController::class, 'store'])->name('merchant.password.email');
});

Route::prefix('master')->group(function () { 
	Route::get('/banks', [\App\Http\Controllers\API\Master\BankController::class, 'getBanks'])->name('master.bank');
	Route::get('/categories', [\App\Http\Controllers\API\Master\CategoryController::class, 'getCategories'])->name('master.category');
	Route::get('/service-methods', [\App\Http\Controllers\API\Master\ServiceController::class, 'getServiceMethods'])->name('master.service-methods');
	Route::get('/shop/verification-document-types', [\App\Http\Controllers\API\Merchant\Shop\VerificationController::class, 'getVerificationDocuments'])->name('master.shop.verification-document-types');
	Route::get('/operating-hour-days', [\App\Http\Controllers\API\Master\GeneralController::class, 'getOperatingHourDays'])->name('master.master.operating-hour-days');
	Route::get('/states', [\App\Http\Controllers\API\Master\GeneralController::class, 'getStates'])->name('master.master.states');
});

Route::prefix('services')->group(function () { 
	Route::get('subcat/{master_sub_category_id}', [\App\Http\Controllers\API\User\Service\ServiceController::class, 'getServicesBySubCategory'])->name('user.services.by.subcategory');
});

Route::prefix('cart')->group(function () { 
	Route::post('add', [\App\Http\Controllers\API\User\Cart\CartController::class, 'addToCart'])->name('user.cart.add');
	Route::get('view', [\App\Http\Controllers\API\User\Cart\CartController::class, 'viewCart'])->name('user.cart.view');
});
