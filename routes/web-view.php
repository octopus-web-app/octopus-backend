<?php

use Illuminate\Support\Facades\Route;

Route::prefix('payment')->group(function () { 
    Route::get('/verification', [\App\Http\Controllers\WebView\Merchant\PaymentController::class, 'verification'])->name('payment.verification');
    Route::post('/verification/response', [\App\Http\Controllers\WebView\Merchant\PaymentController::class, 'response'])->name('payment.response');
});

