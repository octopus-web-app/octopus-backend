<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('profile')->group(function () { 
	Route::get('/', [\App\Http\Controllers\API\User\Profile\ProfileController::class, 'getProfile'])->name('user.profile');
	Route::post('/add-address', [\App\Http\Controllers\API\User\Profile\ProfileController::class, 'addAddress'])->name('user.profile.address.add');
});

