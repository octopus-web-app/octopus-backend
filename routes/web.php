<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MyController;

//use App\Http\Livewire\OrderForm;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache', function() {
    \Artisan::call('config:clear');
    \Artisan::call('route:clear');
    \Artisan::call('view:clear');
    return 'DONE'; 
});

Route::get('/run-command', function() {
    \Artisan::call('generate:times');
    return 'DONE'; 
});

//Route::get('/dashboard', [MyController::class, 'dashboard'])->name('dashboard');
Route::get('/test-query-time', [MyController::class, 'queryTime'])->name('query-time');

//Route::get('/', [MyController::class, 'dashboard'])->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {  

    Route::POST('ipay-response', [MyController::class, 'ipayResponse'])->name('ipay.response');
    Route::POST('ipay-backend', [MyController::class, 'ipayBackend'])->name('ipay.backend');

    Route::get('file/show/{filename}', [MyController::class, 'displayImage'])->name('file.display');
    Route::get('/blank-page', [MyController::class, 'blankTemplate']);
 
    Route::get('/faq', [MyController::class, 'faq'])->name('faq');
    Route::get('/contact-us', [MyController::class, 'contactUs'])->name('contact.us');

});

// Route::prefix('merchant')->group(function () { 
//     Route::post('/reset-password', [\App\Http\Controllers\Merchant\Auth\NewPasswordController::class, 'store'])
//         ->middleware(['guest'])
//         ->name('merchant.password.update');
// });
