<!DOCTYPE html>
<html>
    <head>
        <title>Page Title</title>
    </head>
    <body>
        <form id="myForm" method="post" action="{{ $route }}">
            @foreach($entities as $key => $entity)
            <input type="hidden" name="{{ $key }}" value="{{ $entity }}">
            @endforeach
        </form>
        <script>
            (function() {
                document.getElementById("myForm").submit();
            })();
        </script>
    </body>
</html>

