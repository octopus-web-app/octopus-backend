@if(session('csrf_error')) 
    <div class="container">
        <div class="alert alert-danger" role="alert">
            {{session('csrf_error')}}
        </div>
    </div> 
@endif
@if(session('success')) 
<div class="container">
    <div class="alert alert-success" role="alert">
        {{session('success')}}
    </div>
</div> 
@endif
@if(session('error'))  
<div class="container">
    <div class="alert alert-danger" role="alert">
        {{session('error')}}
    </div>
</div>  
@endif 